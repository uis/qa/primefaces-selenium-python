# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.1.1] - 2023-07-14

### Added

- Interactions for components:
  - SplitButton

## [0.1.0] - 2023-07-05

### Added

- Interactions for components:
  - AccordionPanel
  - AutoComplete
  - BlockUI
  - Button
  - Calendar
  - CascadeSelect
  - CommandButton
  - CommandLink
  - ConfirmDialog
  - ConfirmPopup
  - Datatable
  - DataView
  - DatePicker
  - Dialog
  - FileUpload
  - InputText
  - SelectManyCheckbox
  - SelectOneMenu
  - ToggleSwitch
- Primefaces-specific expected condition wait definitions
- Simple test for selectOneMenu
