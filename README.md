# Primefaces Selenium for Python

[Primefaces](https://www.primefaces.org/) is a UI component library for Java applications.
This library allows [Selenium](https://www.selenium.dev/) UI tests written in Python to test these components effectively.

## Getting started

Each Primefaces component has its own class with methods available to interact specifically with that component.

Each component class needs two arguments:

- a WebDriver (or EventFiringWebDriver) instance
- the base element locator for the component

```python
from selenium.webdriver.common.by import By

from primefaces.component.select_one_menu import SelectOneMenu

def test_select_one_menu_basic_click(driver):
    driver.get("http://www.example.com")
    base_element = (By.ID, "base_element_id")
    select_one = SelectOneMenu(driver, base_element)
    select_one.select("Option Value")
```

The following components are available:

| Primefaces component | UI component class example                     |
| -------------------- | ---------------------------------------------- |
| [accordionpanel]     | AccordionPanel(driver, (By.\*, "locator"))     |
| [autocomplete]       | AutoComplete(driver, (By.\*, "locator"))       |
| [blockui]            | BlockUI(driver, (By.\*, "locator"))            |
| [button]             | Button(driver, (By.\*, "locator"))             |
| [calendar]           | Calendar(driver, (By.\*, "locator"))           |
| [cascadeselect]      | CascadeSelect(driver, (By.\*, "locator"))      |
| [commandbutton]      | CommandButton(driver, (By.\*, "locator"))      |
| [commandlink]        | CommandLink(driver, (By.\*, "locator"))        |
| [confirmdialog]      | ConfirmDialog(driver, (By.\*, "locator"))      |
| [confirmpopup]       | ConfirmPopup(driver, (By.\*, "locator"))       |
| [datatable]          | DataTable(driver, (By.\*, "locator"))          |
| [dataview]           | DataView(driver, (By.\*, "locator"))           |
| [datepicker]         | DatePicker(driver, (By.\*, "locator"))         |
| [dialog]             | Dialog(driver, (By.\*, "locator"))             |
| [fileupload]         | FileUpload(driver, (By.\*, "locator"))         |
| [inputnumber]        | InputNumber(driver, (By.\*, "locator"))        |
| [inputtext]          | InputText(driver, (By.\*, "locator"))          |
| [picklist]           | PickList(driver, (By.\*, "locator"))           |
| [selectmanycheckbox] | SelectManyCheckbox(driver, (By.\*, "locator")) |
| [selectonemenu]      | SelectOneMenu(driver, (By.\*, "locator"))      |
| [spinner]            | Spinner(driver, (By.\*, "locator"))            |
| [splitbutton]        | SplitButton(driver, (By.\*, "locator"))        |
| [tabview]            | TabView(driver, (By.\*, "locator"))            |
| [texteditor]         | TextEditor(driver, (By.\*, "locator"))         |
| [toggleswitch]       | ToggleSwitch(driver, (By.\*, "locator"))       |

[accordionpanel]: https://www.primefaces.org/showcase/ui/panel/accordionPanel.xhtml
[autocomplete]: https://www.primefaces.org/showcase/ui/input/autoComplete.xhtml
[blockui]: https://www.primefaces.org/showcase/ui/misc/blockUI.xhtml
[button]: https://www.primefaces.org/showcase/ui/button/button.xhtml
[calendar]: https://www.primefaces.org/showcase/ui/input/datepicker/datePicker.xhtml
[cascadeselect]: https://www.primefaces.org/showcase/ui/input/cascadeSelect.xhtml
[commandbutton]: https://www.primefaces.org/showcase/ui/button/commandButton.xhtml
[commandlink]: https://www.primefaces.org/showcase/ui/button/commandLink.xhtml
[confirmdialog]: https://www.primefaces.org/showcase/ui/overlay/confirmDialog.xhtml
[confirmpopup]: https://www.primefaces.org/showcase/ui/overlay/confirmPopup.xhtml
[datatable]: https://www.primefaces.org/showcase/ui/data/datatable/basic.xhtml
[dataview]: https://www.primefaces.org/showcase/ui/data/dataview/basic.xhtml
[datepicker]: https://www.primefaces.org/showcase/ui/input/datepicker/datePicker.xhtml
[dialog]: https://www.primefaces.org/showcase/ui/overlay/dialog.xhtml
[fileupload]: https://www.primefaces.org/showcase/ui/file/upload/basic.xhtml
[inputtext]: https://www.primefaces.org/showcase/ui/input/inputText.xhtml
[inputnumber]: https://www.primefaces.org/showcase/ui/input/inputNumber.xhtml
[picklist]: http://www.primefaces.org:8080/showcase/ui/data/pickList.xhtml
[selectmanycheckbox]: https://www.primefaces.org/showcase/ui/input/manyCheckbox.xhtml
[selectonemenu]: https://www.primefaces.org/showcase/ui/input/oneMenu.xhtml
[spinner]: https://www.primefaces.org/showcase/ui/input/spinner.xhtml
[splitbutton]: https://www.primefaces.org/showcase/ui/button/splitButton.xhtml
[tabview]: https://www.primefaces.org/showcase/ui/panel/tabView.xhtml
[texteditor]: https://www.primefaces.org/showcase/ui/input/textEditor.xhtml
[toggleswitch]: https://www.primefaces.org/showcase/ui/input/toggleSwitch.xhtml

## Support

Raise an issue in this project with any issue or questions.

## Project status

This project is in Alpha.
It is currently implementing the components needed for active test development on internal projects.
The project aims to implement all the components provided by Selenium over time.
