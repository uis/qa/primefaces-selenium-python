import time
from functools import cached_property

from selenium.webdriver import Keys
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait

from primefaces.internal.config_provider import ConfigProvider
from primefaces.internal.guard import Guard
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions


class PrimefacesSelenium:
    def __init__(self, web_driver: WebDriver):
        self.web_driver = web_driver
        self.capabilities = self.web_driver.capabilities

    @cached_property
    def is_chrome(self) -> bool:
        return self.capabilities["browserName"].lower() == "chrome"

    @cached_property
    def is_firefox(self) -> bool:
        return self.capabilities["browserName"].lower() == "firefox"

    @cached_property
    def is_safari(self) -> bool:
        return self.capabilities["browserName"].lower() == "safari"

    @cached_property
    def is_mac_os(self) -> bool:
        return self.capabilities["platformName"].lower() in [
            "mac",
            "darwin",
            "macoS",
            "mac os x",
            "os x",
        ]

    @cached_property
    def command_key(self):
        """
        Get the command key for the platform under test.
        :return: command key: Keys
        """
        return Keys.COMMAND if self.is_mac_os else Keys.CONTROL

    @staticmethod
    def has_css_class(element: WebElement, *css_classes) -> bool:
        element_class = element.get_attribute("class")
        if not element_class:
            return False
        element_classes = element_class.split(" ")
        return not any(
            all(actual.lower() != expected.lower() for actual in element_classes)
            for expected in css_classes
        )

    @staticmethod
    def has_style(element: WebElement, *styles) -> bool:
        element_style = element.get_attribute("style")
        if not element_style:
            return False
        element_styles = element_style.split(" ")
        for expected in styles:
            for actual in element_styles:
                if actual.lower().startswith(expected.lower()):
                    return True
        return False

    def element_has_css_classes(self, element: WebElement, *css_classes):
        return all(self.has_css_class(element, css_class) for css_class in css_classes)

    def is_visible_in_viewport(self, element: WebElement) -> bool:
        if not element.is_displayed():
            return False
        return self.web_driver.execute_script(
            f"""
            var elem = {element},
                    box = elem.getBoundingClientRect(),
                    cx = box.left + box.width / 2,
                    cy = box.top + box.height / 2,
                    e = document.elementFromPoint(cx, cy);
                for (; e; e = e.parentElement) {{
                    if (e === elem) {{ return true; }}
                }}
                return false;
            """
        )

    def guard_http(self, target_method):
        return Guard(self.web_driver).http(target_method)

    def guard_ajax_for_script(self, script, *args):
        return Guard(self.web_driver).guard_ajax_for_script(script, *args)

    def guard_ajax(self, target, *args):
        return Guard(self.web_driver).ajax(target, *args)

    def clear_input(self, element: WebElement, is_ajaxified: bool):
        """
        Clears the input field of text.
        See <a href="https://stackoverflow.com/a/64067604/502366">Safari Hack</a>

        :param element: the WebElement input to set: WebElement
        :param is_ajaxified: true if using AJAX: bool
        """
        if self.is_safari:
            if input_text := element.get_attribute("value"):
                clear_text = str([Keys.BACKSPACE for _ in range(len(input_text))])
                if is_ajaxified:
                    self.guard_ajax(element.send_keys, clear_text)
                else:
                    element.send_keys(clear_text)
        else:
            if is_ajaxified:
                self.guard_ajax(element.clear)
            else:
                element.clear()
            element.send_keys(Keys.BACKSPACE)

    def execute_script_ajaxified(self, script, *args):
        return self.guard_ajax_for_script(script, *args)

    def execute_script(self, script, *args):
        """Executes JavaScript in the browser and will wait if the request is an AJAX request."""
        result = self.web_driver.execute_script(script, *args)
        if self.is_safari:
            time.sleep(0.05)
        return result

    def wait_gui(self):
        """
        Wait will ignore instances of NotFoundException that are encountered (thrown) by default
        in the 'until' condition, and immediately propagate all others.
        You can add more to the ignore list by calling ignoring(exceptions to add).
        """
        return WebDriverWait(self.web_driver, ConfigProvider.TIMEOUT_DOCUMENT_LOAD, 0.1)

    def wait_document_load(self):
        """Wait until the document is loaded."""
        wait = WebDriverWait(self.web_driver, ConfigProvider.TIMEOUT_DOCUMENT_LOAD, 0.1)
        wait.until(PrimefacesExpectedConditions().document_loaded())
        return wait

    def disable_animations(self):
        """Globally disable all CSS and jQuery animations."""
        self.execute_script(
            "if (window.PrimeFaces) { $(function() { PrimeFaces.utils.disableAnimations(); }); }"
        )

    def enable_animations(self):
        """Globally enable all CSS and jQuery animations."""
        self.execute_script(
            "if (window.PrimeFaces) { $(function() { PrimeFaces.utils.enableAnimations(); }); }"
        )

    def set_hidden_input(self, element: WebElement, value: str):
        self.execute_script(
            f" document.getElementById('{element.get_attribute('id')}').value='{value}'"
        )
