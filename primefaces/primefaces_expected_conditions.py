from collections.abc import Callable
from pathlib import Path

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions


class PrimefacesExpectedConditions:
    @staticmethod
    def script(script: str):
        return lambda d: d.execute_script(script)

    def document_loaded(self):
        return self.script("return document.readyState === 'complete'")

    def not_navigating(self):
        return self.script("return (!window.pfselenium || pfselenium.navigating === false);")

    def not_submitting(self):
        return self.script("return (!window.pfselenium || pfselenium.submitting === false);")

    def animation_not_active(self):
        return self.script(
            """return (
            (
            !window.jQuery
            || jQuery.active == 0
            ) && (
            !window.PrimeFaces
            || PrimeFaces.animationActive === false
            || PrimeFaces.animationActive === undefined
            )
            );"""
        )

    def ajax_queue_empty(self):
        return self.script("return (!window.PrimeFaces || PrimeFaces.ajax.Queue.isEmpty());")

    def visible_and_animation_complete(self, element: WebElement):
        return expected_conditions.all_of(
            self.document_loaded(),
            self.animation_not_active(),
            self.ajax_queue_empty(),
            expected_conditions.visibility_of(element),
        )

    def invisible_and_animation_complete(self, element: WebElement):
        return expected_conditions.all_of(
            self.document_loaded(),
            self.animation_not_active(),
            self.ajax_queue_empty(),
            expected_conditions.invisibility_of_element(element),
        )

    @staticmethod
    def visible_in_viewport(element: WebElement):
        from primefaces.primefaces_selenium import PrimefacesSelenium

        return lambda d: PrimefacesSelenium(d).is_visible_in_viewport(element)

    @staticmethod
    def file_exists(file_path: Path):
        return lambda d: file_path.is_file()

    @staticmethod
    def staleness_of(element: WebElement) -> Callable[[WebDriver], bool]:
        """Wait until an element is no longer attached to the DOM.

        element is the element to wait for. returns False if the element is
        still attached to the DOM, true otherwise.
        """

        def _predicate(_):
            try:
                # Calling any method forces a staleness check
                element.is_enabled()
                return False
            except Exception:
                return True

        return _predicate
