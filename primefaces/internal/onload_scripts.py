from selenium.webdriver.remote.webdriver import WebDriver

from primefaces.internal.config_provider import ConfigProvider


class OnloadScripts:
    def __init__(self, web_driver: WebDriver):
        self.web_driver = web_driver
        from primefaces.primefaces_selenium import PrimefacesSelenium

        self.ps = PrimefacesSelenium(self.web_driver)

    def is_installed(self):
        self.ps.wait_document_load()
        return self.ps.execute_script("return window.pfselenium != null;")

    def execute(self):
        if self.is_installed():
            # print("Installed!")
            return
        # print("Installing...")
        # traceback.print_stack()
        onload_scripts = ConfigProvider.ONLOAD_SCRIPTS
        string_script = ";".join(onload_scripts)
        self.ps.execute_script(f"(function () {{ {string_script} }})();")
        # print("Just installed!")
