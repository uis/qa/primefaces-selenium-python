import os


class ConfigProvider:
    TIMEOUT_GUI = int(os.getenv("PRIMEFACES_TIMEOUT_GUI", 5))
    TIMEOUT_AJAX = int(os.getenv("PRIMEFACES_TIMEOUT_AJAX", 10))
    TIMEOUT_HTTP = int(os.getenv("PRIMEFACES_TIMEOUT_HTTP", 10))
    TIMEOUT_DOCUMENT_LOAD = int(os.getenv("PRIMEFACES_TIMEOUT_DOCUMENT_LOAD", 15))
    TIMEOUT_FILE_UPLOAD = int(os.getenv("PRIMEFACES_TIMEOUT_FILE_UPLOAD", 20))
    SCROLL_ELEMENT_INTO_VIEW: str = os.getenv("PRIMEFACES_SCRIPT_ELEMENT_INTO_VIEW", "True")
    DISABLE_ANIMATIONS = bool(os.getenv("PRIMEFACES_DISABLE_ANIMATIONS", False))
    ONLOAD_SCRIPTS = []
    with open(os.path.join(os.path.dirname(__file__), "onload.js")) as onload_file:
        ONLOAD_SCRIPTS.append("".join(onload_file.readlines()))
    if DISABLE_ANIMATIONS:
        ONLOAD_SCRIPTS.append(
            """
            if (window.PrimeFaces) {
             $(function() { PrimeFaces.utils.disableAnimations(); });
             }
             """
        )
        ONLOAD_SCRIPTS.append(
            """document.head.insertAdjacentHTML(
            'beforeend',
            '<style>*,
            *:before,
            *:after {
             -webkit-transition: none !important;
             -moz-transition: none !important;
             -ms-transition: none !important;
             -o-transition: none !important;
             transition: none !important;
             }
             </style>');"""
        )
    if SCROLL_ELEMENT_INTO_VIEW.lower() in {"true", "1", "t"}:
        ONLOAD_SCRIPTS.append(
            """if (window.PrimeFaces) {
             $(function() {
             PrimeFaces.hideOverlaysOnViewportChange = false;
             });
             }"""
        )
