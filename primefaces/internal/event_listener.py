from selenium.webdriver.ie.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.abstract_event_listener import AbstractEventListener

from primefaces.internal.onload_scripts import OnloadScripts
from primefaces.primefaces_selenium import PrimefacesSelenium


class EventListener(AbstractEventListener):
    def before_click(self, element: WebElement, driver: WebDriver) -> None:
        OnloadScripts(driver).execute()

    def after_click(self, element: WebElement, driver: WebDriver) -> None:
        OnloadScripts(driver).execute()

    def after_navigate_back(self, driver: WebDriver) -> None:
        OnloadScripts(driver).execute()

    def after_navigate_to(self, url: str, driver: WebDriver) -> None:
        OnloadScripts(driver).execute()

    def before_navigate_back(self, driver: WebDriver) -> None:
        OnloadScripts(driver).execute()

    def before_navigate_forward(self, driver: WebDriver) -> None:
        OnloadScripts(driver).execute()

    def before_navigate_to(self, url: str, driver: WebDriver) -> None:
        OnloadScripts(driver).execute()

    def after_navigate_forward(self, driver: WebDriver) -> None:
        OnloadScripts(driver).execute()


class ScrollElementIntoViewClickListener(AbstractEventListener):
    def __init__(self, option: str | None = None):
        self.option = option

    def before_click(self, element: WebElement, driver: WebDriver) -> None:
        ps = PrimefacesSelenium(driver)
        if not element.is_displayed():
            return
        if ps.is_visible_in_viewport(element):
            return
        ps.execute_script(f"arguments[0].scrollIntoView({self.option})", element)
