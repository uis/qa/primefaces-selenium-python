import time
from collections.abc import Callable

from selenium.common import TimeoutException
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from primefaces.internal.config_provider import ConfigProvider
from primefaces.internal.onload_scripts import OnloadScripts
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions


class Guard:
    def __init__(self, web_driver: WebDriver):
        self.web_driver = web_driver

    def wait_until_ajax_completes(self):
        WebDriverWait(self.web_driver, ConfigProvider.TIMEOUT_AJAX, 0.05).until(
            lambda d: d.execute_script(
                """
            return document.readyState==='complete'
             && (!window.jQuery || jQuery.active==0)
             && (!window.PrimeFaces || (PrimeFaces.ajax.Queue.isEmpty()
             && (PrimeFaces.animationActive===false || PrimeFaces.animationActive===undefined)))
             && (!window.pfselenium || (pfselenium.xhr==null && pfselenium.navigating===false));
            """
            )
        )

    def get_ajax_debug_info(self):
        scripts = [
            "return document.readyState;",
            "return !window.jQuery;",
            "return jQuery.active;",
            "return !window.PrimeFaces;",
            "return PrimeFaces.ajax.Queue.isEmpty();",
            "return PrimeFaces.animationActive;",
            "return !window.pfselenium;",
            "return pfselenium.xhr;",
            "return pfselenium.anyXhrStarted;",
            "return pfselenium.navigating;",
        ]

        results = []
        for script in scripts:
            try:
                result = self.web_driver.execute_script(script)
            except Exception as e:
                result = f"Error: {str(e)}"
            results.append(result)

        debug_info = (
            f"document.readyState={results[0]}, "
            f"!window.jQuery={results[1]}, "
            f"jQuery.active={results[2]}, "
            f"!window.PrimeFaces={results[3]}, "
            f"PrimeFaces.ajax.Queue.isEmpty()={results[4]}, "
            f"PrimeFaces.animationActive={results[5]}, "
            f"!window.pfselenium={results[6]}, "
            f"pfselenium.xhr={results[7]}, "
            f"pfselenium.anyXhrStarted={results[8]}, "
            f"pfselenium.navigating={results[9]}"
        )

        return debug_info

    def guard_ajax_for_script(self, script: str, *args):
        OnloadScripts(self.web_driver).execute()
        try:
            self.web_driver.execute_script("pfselenium.xhr = 'somethingJustNotNull';")
            result = self.web_driver.execute_script(script, *args)
            self.wait_until_ajax_completes()
            return result
        except TimeoutException as e:
            raise TimeoutException("Timeout while waiting for AJAX complete!") from e

    def ajax(self, target, *args):
        OnloadScripts(self.web_driver).execute()
        try:
            self.web_driver.execute_script("pfselenium.xhr = 'somethingJustNotNull';")
            result = target(*args) if args else target()
            # print(self.get_ajax_debug_info())
            self.wait_until_ajax_completes()
            # print(self.get_ajax_debug_info())
            return result
        except TimeoutException as e:
            raise TimeoutException("Timeout while waiting for AJAX complete!") from e

    def http(self, target_method):
        OnloadScripts(self.web_driver).execute()
        try:
            self.web_driver.execute_script("pfselenium.xhr = 'somethingJustNotNull';")
            result = target_method()
            WebDriverWait(self.web_driver, ConfigProvider.TIMEOUT_AJAX, 0.05).until(
                expected_conditions.all_of(
                    PrimefacesExpectedConditions().document_loaded(),
                    PrimefacesExpectedConditions().not_navigating(),
                    PrimefacesExpectedConditions().not_submitting(),
                )
            )
            return result
        except TimeoutException as e:
            raise TimeoutException("Timeout while waiting for document ready!") from e
        except Exception as e:
            raise e

    def custom(self, target_method: Callable, delay: float, timeout: float, *args):
        OnloadScripts(self.web_driver).execute()
        try:
            self.web_driver.execute_script("pfselenium.xhr = 'somethingJustNotNull';")
            result = target_method()
            if delay is not None:
                time.sleep(delay)
            WebDriverWait(self.web_driver, timeout, 0.1).until(expected_conditions.all_of(*args))
            return result
        except TimeoutException as e:
            raise TimeoutException("Timeout while waiting for document ready!") from e
        except Exception as e:
            raise e
