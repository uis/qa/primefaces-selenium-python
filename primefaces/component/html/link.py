from primefaces.component.base.abstract_component import AbstractComponent
from primefaces.primefaces_selenium import PrimefacesSelenium


class Link(AbstractComponent):
    def get_href(self):
        return self.root_element.get_attribute("href")

    def get_target(self):
        return self.root_element.get_attribute("target")

    def click(self):
        href = self.get_href()
        target = self.get_target()

        if target == "_blank" or href is not None and href.startswith("#"):
            self.root_element.click()
        else:
            PrimefacesSelenium(self.web_driver).guard_http(self.root_element).click()
