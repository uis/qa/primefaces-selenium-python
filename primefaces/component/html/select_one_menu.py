from functools import cached_property

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selocity import resilient_cached_webelements

from primefaces.component.base.abstract_input_component import AbstractInputComponent
from primefaces.primefaces_selenium import PrimefacesSelenium


class SelectOneMenu(AbstractInputComponent):
    @property
    @resilient_cached_webelements
    def option_elements(self) -> list[WebElement]:
        return self.root_element.find_elements(By.TAG_NAME, "option")

    @cached_property
    def labels(self) -> list[str]:
        return [e.get_attribute("innerHTML") for e in self.option_elements]

    def get_label(self, index: int):
        return self.labels[index]

    def get_selected_label(self):
        for option in self.option_elements:
            if option.is_selected():
                return option.get_attribute("innerHTML")
        return None

    def is_selected_by_index(self, index: int) -> bool:
        return self.get_label(index) == self.get_selected_label()

    def is_selected_by_label(self, label: str) -> bool:
        return self.get_selected_label().lower() == label.lower()

    def deselect(self, label: str = "", index: int | None = None):
        if index is not None:
            if not self.is_selected_by_index(index):
                return
            label = self.get_label(index)

        if not self.is_selected_by_label(label) or not self.is_enabled():
            return

        for option in self.option_elements:
            if option.get_attribute("innerHTML").strip() == label and option.is_selected():
                if self.is_ajaxified("onchange"):
                    PrimefacesSelenium(self.web_driver).guard_ajax(option.click)
                else:
                    option.click()
                return

    def select(self, label: str = "", index: int | None = None):
        if index is not None:
            if self.is_selected_by_index(index):
                return
            label = self.get_label(index)

        if self.is_selected_by_label(label) or not self.is_enabled():
            return

        for option in self.option_elements:
            if option.get_attribute("innerHTML").strip() == label and not option.is_selected():
                if self.is_ajaxified("onchange"):
                    PrimefacesSelenium(self.web_driver).guard_ajax(option.click)
                else:
                    option.click()
                return
