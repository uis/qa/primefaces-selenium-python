from functools import cached_property

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selocity import resilient_cached_webelement, resilient_cached_webelements

from primefaces.component.base.abstract_input_component import AbstractInputComponent
from primefaces.component.base.component_utils import ComponentUtils
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions
from primefaces.primefaces_selenium import PrimefacesSelenium


class SelectOneMenu(AbstractInputComponent):
    @cached_property
    def is_editable(self):
        return self.widget_configuration.get("editable")

    @cached_property
    def labels(self):
        if self.widget_configuration.get("filter"):
            # element.is_displayed() only works when panel is visible
            self.show()
            return [
                element.get_attribute("innerHTML")
                for element in self.item_elements
                if element.is_displayed()
            ]
        return [element.get_attribute("innerHTML") for element in self.item_elements]

    @property
    @resilient_cached_webelement
    def input_element(self) -> WebElement:
        return self.get_by_partial_id_from_base_element("_input")

    @property
    @resilient_cached_webelement
    def panel_element(self) -> WebElement:
        return self.get_by_partial_id_from_root("_panel")

    @property
    @resilient_cached_webelement
    def filter_input_element(self) -> WebElement:
        return self.get_by_partial_id_from_root("_filter")

    @property
    @resilient_cached_webelement
    def label_element(self) -> WebElement:
        return self.root_element.find_element(value=f"{self.root_dom_id}_label")

    @property
    @resilient_cached_webelement
    def editable_input_element(self):
        return self.root_element.find_element(value=f"{self.root_dom_id}_label")

    @property
    @resilient_cached_webelement
    def items_element(self) -> WebElement:
        return self.web_driver.find_element(value=f"{self.root_dom_id}_items")

    @property
    @resilient_cached_webelements
    def item_elements(self) -> list[WebElement]:
        return self.items_element.find_elements(By.CSS_SELECTOR, "li.ui-selectonemenu-item")

    @property
    @resilient_cached_webelement
    def assigned_label_element(self):
        return self.web_driver.find_element(
            By.CSS_SELECTOR,
            f"label[for='{self.root_dom_id}"
            + ("_focus" if self.is_editable() else "_label")
            + "']",
        )

    def is_selected_by_label(self, label) -> bool:
        return self.get_selected_label().lower() == label.lower()

    def is_item_select_ajaxified(self):
        return ComponentUtils(self.web_driver).has_ajax_behavior(self.root_element, "itemSelect")

    def hide(self):
        if self.panel_element.is_displayed():
            PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.hide();")
            PrimefacesSelenium(self.web_driver).wait_gui().until(
                PrimefacesExpectedConditions().invisible_and_animation_complete(self.panel_element)
            )

    def show(self):
        if not self.panel_element.is_displayed():
            PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.show();")
            PrimefacesSelenium(self.web_driver).wait_gui().until(
                PrimefacesExpectedConditions().visible_and_animation_complete(self.panel_element)
            )

    def click_option(self, element: WebElement):
        if self.is_on_change_ajaxified() or self.is_item_select_ajaxified():
            PrimefacesSelenium(self.web_driver).guard_ajax(element.click)
        else:
            element.click()
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            PrimefacesExpectedConditions().animation_not_active()
        )
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            expected_conditions.invisibility_of_element(self.panel_element)
        )

    def select(self, label: str, partial_match: bool = False):
        if self.is_selected_by_label(label) or not self.is_enabled():
            return
        self.show()
        for option in self.item_elements:
            _option_text = option.get_attribute("textContent").lower()
            if (partial_match and label.lower() in _option_text) or (
                not partial_match and _option_text == label.lower()
            ):
                self.click_option(option)
                break
        self.hide()

    def deselect(self, label: str, partial_match: bool = False):
        if not self.is_selected_by_label(label) or not self.is_enabled():
            return
        self.show()
        for option in self.item_elements:
            _option_text = option.get_attribute("textContent").lower()
            if (partial_match and label.lower() in _option_text) or (
                not partial_match and _option_text == label.lower()
            ):
                self.click_option(option)
                break
        self.hide()

    def get_selected_label(self) -> str:
        return self.label_element.get_attribute("textContent")
