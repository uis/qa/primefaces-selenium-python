from selenium.common import NoSuchElementException
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.event_firing_webdriver import EventFiringWebDriver
from selocity import resilient_cached_webelement

from primefaces.component.base.abstract_component import AbstractComponent
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions
from primefaces.primefaces_selenium import PrimefacesSelenium


class PickList(AbstractComponent):
    ITEM_LABEL_ATTR = "data-item-label"
    ITEM_VALUE_ATTR = "data-item-value"
    CHECK_SELECTOR = (By.CSS_SELECTOR, "div[class*='ui-chkbox']")

    def __init__(
        self,
        web_driver: WebDriver | EventFiringWebDriver,
        base_element_locator: tuple[str, str],
    ):
        super().__init__(web_driver, base_element_locator)
        self.primefaces_selenium = PrimefacesSelenium(self.web_driver)

    @property
    @resilient_cached_webelement
    def add_element(self):
        return self.root_element.find_element(By.CLASS_NAME, "ui-picklist-button-add")

    @property
    @resilient_cached_webelement
    def add_all_element(self):
        return self.root_element.find_element(By.CLASS_NAME, "ui-picklist-button-add-all")

    @property
    @resilient_cached_webelement
    def remove_element(self):
        return self.root_element.find_element(By.CLASS_NAME, "ui-picklist-button-remove")

    @property
    @resilient_cached_webelement
    def remove_all_element(self):
        return self.root_element.find_element(By.CLASS_NAME, "ui-picklist-button-remove-all")

    @property
    @resilient_cached_webelement
    def source_list_element(self):
        return self.root_element.find_element(By.CSS_SELECTOR, "ul[class*='ui-picklist-source']")

    @property
    @resilient_cached_webelement
    def target_list_element(self):
        return self.root_element.find_element(By.CSS_SELECTOR, "ul[class*='ui-picklist-target']")

    @property
    @resilient_cached_webelement
    def source_filter_element(self):
        return self.root_element.find_element(By.CSS_SELECTOR, "[id$='_source_filter']")

    @property
    @resilient_cached_webelement
    def target_filter_element(self):
        return self.root_element.find_element(By.CSS_SELECTOR, "[id$='_target_filter']")

    def _select_and_action(self, list_element: WebElement, action_button: WebElement, *items):
        has_checkboxes = self.is_checkbox_selection_enabled()
        _list_items = list_element.find_elements(By.TAG_NAME, "li")
        _item_labels = list(items).copy()

        def _pick(element):
            if has_checkboxes:
                checkbox = self.get_checkbox(element)
                checkbox.click()
                if action_button.is_enabled():
                    action_button.click()
            else:
                element.click()
                action_button.click()

        for list_item_element in _list_items:
            value = list_item_element.get_attribute(self.ITEM_VALUE_ATTR)
            label = list_item_element.get_attribute(self.ITEM_LABEL_ATTR)

            if value in _item_labels:
                _pick(list_item_element)
                _item_labels.remove(value)
            elif label in _item_labels:
                _pick(list_item_element)
                _item_labels.remove(label)

            if not _item_labels:
                break

        if _item_labels:
            raise NoSuchElementException(
                f"No elements with labels {_item_labels} in picklist {self.root_dom_id}"
            )

    def pick(self, *items):
        self._select_and_action(self.source_list_element, self.add_element, *items)

    def pick_all(self):
        self.add_all_element.click()

    def remove_items(self, *items):
        self._select_and_action(self.target_list_element, self.remove_element, *items)

    def remove_all_items(self):
        self.remove_all_element.click()

    def get_checkbox(self, element):
        checkboxes = element.find_elements(*self.CHECK_SELECTOR)
        return checkboxes[0] if checkboxes else None

    def get_source_list_elements(self):
        return [
            element
            for element in self.source_list_element.find_elements(By.TAG_NAME, "li")
            if element.is_displayed()
        ]

    def get_filtered_source_list_elements(self):
        source_list = self.get_source_list_elements()
        return [
            element
            for element in source_list
            if "display: none;" not in element.get_attribute("style")
        ]

    def get_source_list_values(self):
        return [
            element.get_attribute(self.ITEM_VALUE_ATTR)
            for element in self.get_source_list_elements()
        ]

    def get_source_list_labels(self):
        return [
            element.get_attribute(self.ITEM_LABEL_ATTR)
            for element in self.get_source_list_elements()
        ]

    def get_target_list_elements(self):
        return [
            element
            for element in self.target_list_element.find_elements(By.TAG_NAME, "li")
            if element.is_displayed()
        ]

    def get_filtered_target_list_elements(self):
        target_list = self.get_target_list_elements()
        return [
            element
            for element in target_list
            if "display: none;" not in element.get_attribute("style")
        ]

    def get_target_list_values(self):
        return [
            element.get_attribute(self.ITEM_VALUE_ATTR)
            for element in self.get_target_list_elements()
        ]

    def get_target_list_labels(self):
        return [
            element.get_attribute(self.ITEM_LABEL_ATTR)
            for element in self.get_target_list_elements()
        ]

    def is_checkbox_selection_enabled(self):
        if source_elements := self.get_source_list_elements():
            return self.get_checkbox(source_elements[0]) is not None

        if target_elements := self.get_target_list_elements():
            return self.get_checkbox(target_elements[0]) is not None

        return False

    def filter_source_list(self, filter_value):
        self.set_filter_value(self.source_filter_element, filter_value)

    def filter_target_list(self, filter_value):
        self.set_filter_value(self.target_filter_element, filter_value)

    def clear_source_list_filter(self):
        self.set_filter_value(self.source_filter_element, "")

    def clear_target_list_filter(self):
        self.set_filter_value(self.target_filter_element, "")

    def set_filter_value(self, filter_element, filter_value):
        cfg = self.widget_configuration
        filter_event = cfg.get("filterEvent")
        filter_delay = cfg.get("filterDelay")
        self._set_filter_value(filter_element, filter_value, filter_event, filter_delay)

    def _set_filter_value(self, filter_element, filter_value, filter_event, filter_delay):
        self.primefaces_selenium.clear_input(filter_element, False)

        trigger_key = None
        filter_event = filter_event.lower()
        if filter_event in ["keyup", "keydown", "keypress", "input"]:
            if filter_delay == 0:
                filter_element = self.primefaces_selenium.guard_ajax(filter_element)
        elif filter_event == "enter":
            trigger_key = Keys.ENTER
        elif filter_event in ["change", "blur"]:
            trigger_key = Keys.TAB

        if filter_value is not None:
            filter_element.send_keys(filter_value)
        else:
            filter_element.send_keys(Keys.BACK_SPACE)

        if trigger_key is not None:
            filter_element.send_keys(trigger_key)
        elif filter_delay > 0:
            self.primefaces_selenium.wait_gui().until(
                PrimefacesExpectedConditions().animation_not_active()
            )
