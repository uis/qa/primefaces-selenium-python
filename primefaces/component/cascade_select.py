from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selocity import resilient_cached_webelement, resilient_cached_webelements

from primefaces.component.base.abstract_input_component import AbstractInputComponent
from primefaces.component.base.component_utils import ComponentUtils
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions
from primefaces.primefaces_selenium import PrimefacesSelenium


class CascadeSelect(AbstractInputComponent):
    @property
    @resilient_cached_webelement
    def input_element(self) -> WebElement:
        return self.get_by_partial_id_from_base_element("_input")

    @property
    @resilient_cached_webelement
    def panel_element(self) -> WebElement:
        return self.get_by_partial_id_from_base_element("_panel")

    @property
    @resilient_cached_webelements
    def item_elements(self) -> list[WebElement]:
        return self.panel_element.find_elements(By.CLASS_NAME, "ui-cascadeselect-item")

    @property
    @resilient_cached_webelement
    def label_element(self):
        return self.root_element.find_element(By.CLASS_NAME, "ui-cascadeselect-label")

    @property
    @resilient_cached_webelements
    def leaf_item_elements(self):
        return [
            e
            for e in self.item_elements
            if not PrimefacesSelenium(self.web_driver).has_css_class(
                e, "ui-cascadeselect-item-group"
            )
        ]

    def is_item_select_ajaxified(self):
        return ComponentUtils(self.web_driver).has_ajax_behavior(self.root_element, "itemSelect")

    def show(self):
        if self.is_enabled() and not self.panel_element.is_displayed():
            ps = PrimefacesSelenium(self.web_driver)
            ps.execute_script(f"{self.widget_id}.hide();")
            ps.wait_gui().until(
                PrimefacesExpectedConditions().invisible_and_animation_complete(self.panel_element)
            )

    def hide(self):
        if self.is_enabled() and self.panel_element.is_displayed():
            ps = PrimefacesSelenium(self.web_driver)
            ps.execute_script(f"{self.widget_id}.show();")
            ps.wait_gui().until(
                PrimefacesExpectedConditions().visible_and_animation_complete(self.panel_element)
            )

    def get_labels(self):
        return [e.get_attribute("data-label") for e in self.item_elements]

    def click_element(self, element: WebElement):
        ps = PrimefacesSelenium(self.web_driver)
        if not ps.has_css_class(element, ["ui-cascadeselect-item-group"]) and (
            self.is_on_change_ajaxified() or self.is_item_select_ajaxified()
        ):
            ps.guard_ajax(element.click)
        else:
            element.click()

    def is_selected_by_label(self, label) -> bool:
        return self.get_selected_label().lower() == label.lower()

    def select(self, label: str):
        if self.is_selected_by_label(label) or not self.is_enabled():
            return
        self.show()
        for element in self.item_elements:
            if element.get_attribute("data-label").lower() == label.lower():
                self.click_element(element)
                return

    def get_selected_label(self) -> str:
        return self.label_element.get_attribute("textContent")

    def get_values(self):
        return [e.get_attribute("data-value") for e in self.item_elements]
