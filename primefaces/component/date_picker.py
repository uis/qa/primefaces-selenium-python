import datetime
import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.select import Select
from selocity import resilient_cached_webelement

from primefaces.component.base.abstract_input_component import AbstractInputComponent
from primefaces.component.base.component_utils import ComponentUtils
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions
from primefaces.primefaces_selenium import PrimefacesSelenium


class DatePicker(AbstractInputComponent):
    @property
    @resilient_cached_webelement
    def input_element(self):
        return self.get_by_partial_id_from_base_element("_input")

    @property
    @resilient_cached_webelement
    def panel_element(self):
        return self.root_element.find_element(value=f"{self.root_dom_id}_panel")

    def click(self):
        self.input_element.click()
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            PrimefacesExpectedConditions().visible_and_animation_complete(self.panel_element)
        )

    def is_date_select_ajaxified(self):
        return ComponentUtils(self.web_driver).has_ajax_behavior(self.root_element, "dateSelect")

    def is_view_change_ajaxified(self):
        return ComponentUtils(self.web_driver).has_ajax_behavior(
            self.root_element, "viewChange"
        )  #

    def is_close_ajaxified(self):
        return ComponentUtils(self.web_driver).has_ajax_behavior(self.root_element, "close")

    def show_panel(self):
        if self.is_enabled():
            """due to an async setTimeout in hideOverlay"""
            time.sleep(0.11)
            if not self.panel_element.is_displayed():
                PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.show()")
            PrimefacesSelenium(self.web_driver).wait_gui().until(
                PrimefacesExpectedConditions().visible_and_animation_complete(self.panel_element)
            )
        return self.panel_element

    def hide_panel(self):
        if self.is_enabled():
            if self.panel_element.is_displayed():
                if self.is_close_ajaxified():
                    PrimefacesSelenium(self.web_driver).execute_script_ajaxified(
                        f"{self.widget_id}.hide();"
                    )
                else:
                    PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.hide();")
                """due to an async setTimeout in hideOverlay"""
                time.sleep(0.11)
            PrimefacesSelenium(self.web_driver).wait_gui().until(
                PrimefacesExpectedConditions().visible_and_animation_complete(self.panel_element)
            )
        return self.panel_element

    def get_timezone_offset(self):
        return int(
            PrimefacesSelenium(self.web_driver).execute_script(
                "return new Date().getTimezoneOffset();"
            )
        )

    def toggle_month_dropdown(self):
        month_dropdown = self.show_panel().find_element(
            By.CSS_SELECTOR, "select.ui-datepicker-month"
        )
        month_dropdown.click()

    def select_month_dropdown(self, month: int):
        month_dropdown = Select(
            self.show_panel().find_element(By.CSS_SELECTOR, "select.ui-datepicker-month")
        )
        month_dropdown.select_by_value(str(month))

    def toggle_year_dropdown(self):
        year_dropdown = self.show_panel().find_element(
            By.CSS_SELECTOR, "select.ui-datepicker-year"
        )
        year_dropdown.click()

    def select_year_dropdown(self, year: int):
        year_dropdown = Select(
            self.show_panel().find_element(By.CSS_SELECTOR, "select.ui-datepicker-year")
        )
        year_dropdown.select_by_value(str(year))

    def click_next_month_link(self):
        link = self.show_panel().find_element(By.CLASS_NAME, "ui-datepicker-next")
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            expected_conditions.element_to_be_clickable(link)
        )
        if self.is_view_change_ajaxified():
            PrimefacesSelenium(self.web_driver).guard_ajax(link.click)
        else:
            link.click()

    def click_previous_month_link(self):
        link = self.show_panel().find_element(By.CLASS_NAME, "ui-datepicker-prev")
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            expected_conditions.element_to_be_clickable(link)
        )
        if self.is_view_change_ajaxified():
            PrimefacesSelenium(self.web_driver).guard_ajax(link.click)
        else:
            link.click()

    def select_day(self):
        link = self.show_panel().find_element(By.LINK_TEXT, "day")
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            expected_conditions.element_to_be_clickable(link)
        )
        if self.is_view_change_ajaxified():
            PrimefacesSelenium(self.web_driver).guard_ajax(link.click)
        else:
            link.click()

    def get_clear_button(self):
        button = (
            self.show_panel()
            .find_element(By.CLASS_NAME, "ui-datepicker-buttonbar")
            .find_element(By.CLASS_NAME, "ui-clear-button")
        )
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            expected_conditions.element_to_be_clickable(button)
        )
        return button

    def get_today_button(self):
        button = (
            self.show_panel()
            .find_element(By.CLASS_NAME, "ui-datepicker-buttonbar")
            .find_element(By.CLASS_NAME, "ui-today-button")
        )
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            expected_conditions.element_to_be_clickable(button)
        )
        return button

    def has_date(self) -> bool:
        return bool(
            PrimefacesSelenium(self.web_driver).execute_script(
                f"return {self.widget_id}.hasDate();"
            )
        )

    def set_date(self, epoch):
        ps = PrimefacesSelenium(self.web_driver)
        script = f"{self.widget_id}.setDate(new Date({epoch}));"
        if self.is_date_select_ajaxified():
            ps.execute_script_ajaxified(script)
        else:
            ps.execute_script(script)

    def get_widget_date(self):
        return PrimefacesSelenium(self.web_driver).execute_script(
            f"return {self.widget_id}.getDate();"
        )

    def is_lazy(self):
        _id = self.widget_id
        return PrimefacesSelenium(self.web_driver).execute_script(
            f"return {_id}.cfg.lazyDataModel === undefined ? false : {_id}.cfg.lazyDataModel"
        )

    def update_view_date(self, epoch):
        PrimefacesSelenium(self.web_driver).execute_script(
            f"{self.widget_id}.jq.data().primeDatePicker.updateViewDate(null, new Date({epoch}));"
        )

    def get_value(self):
        if not self.has_date():
            return None
        epoch = PrimefacesSelenium(self.web_driver).execute_script(
            f"return {self.widget_id}.getDate().getTime();"
        )
        return datetime.datetime.fromtimestamp(epoch)

    def set_value(self, date_input):
        if isinstance(date_input, datetime.datetime):
            millis = date_input.timestamp()
        else:
            millis = date_input
        self.set_date(millis)
