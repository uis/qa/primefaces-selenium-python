from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selocity import resilient_cached_webelement

from primefaces.component.model.data_table.cell import Cell
from primefaces.component.model.data_table.data_table_row import DataTableRow
from primefaces.primefaces_selenium import PrimefacesSelenium


class TreetableDataTableRow(DataTableRow):
    def __init__(self, web_driver: WebDriver, web_element: WebElement, cells: list[Cell]):
        super().__init__(web_element, cells)
        self.web_driver = web_driver

    @property
    @resilient_cached_webelement
    def toggler_element(self) -> WebElement:
        return self.get_cell(0).web_element.find_element(By.CLASS_NAME, "ui-treetable-toggler")

    def get_level(self) -> int:
        css_classes = self.web_element.get_attribute("class")
        if css_classes:
            if level_class_opt := [
                c for c in css_classes.split(" ") if c.startswith("ui-node-level")
            ]:
                level_class = level_class_opt[0]
                return int(level_class.replace("ui-node-level-", ""))
        return -1

    def is_toggleable(self) -> bool:
        return bool(self.toggler_element)

    def toggle(self) -> None:
        if self.is_toggleable():
            PrimefacesSelenium(self.web_driver).guard_ajax(self.toggler_element.click)
