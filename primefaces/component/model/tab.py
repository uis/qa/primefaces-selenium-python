import dataclasses

from selenium.webdriver.remote.webelement import WebElement


@dataclasses.dataclass
class Tab:
    title: str
    index: int
    header_element: WebElement
    content_element: WebElement
