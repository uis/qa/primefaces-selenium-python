from enum import Enum


class Severity(str, Enum):
    INFO = "info"
    WARN = "warn"
    ERROR = "error"
    FATAL = "fatal"

    @staticmethod
    def to_severity(class_name: str):
        class_name_lower = class_name.lower()
        if "info" in class_name_lower:
            return Severity.INFO.value
        if "warn" in class_name_lower:
            return Severity.WARN.value
        if "error" in class_name_lower:
            return Severity.ERROR.value
        if "fatal" in class_name_lower:
            return Severity.FATAL.value
        return None
