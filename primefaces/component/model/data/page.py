from selenium.webdriver.remote.webelement import WebElement


class Page:
    def __init__(self, number: int, web_element: WebElement):
        self.number = number
        self.web_element = web_element
