from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selocity import resilient_cached_webelement

from primefaces.component.model.data.page import Page
from primefaces.primefaces_selenium import PrimefacesSelenium


class Paginator:
    PAGE = (By.CLASS_NAME, "ui-paginator-page")
    NEXT_PAGE = (By.CLASS_NAME, "ui-paginator-next")
    PREVIOUS_PAGE = (By.CLASS_NAME, "ui-paginator-prev")
    FIRST_PAGE = (By.CLASS_NAME, "ui-paginator-first")
    LAST_PAGE = (By.CLASS_NAME, "ui-paginator-last")

    def __init__(self, web_element: WebElement):
        self.web_element = web_element
        self._pages: list[Page] = []

    @property
    @resilient_cached_webelement
    def next_page_element(self):
        return self.web_element.find_element(*self.NEXT_PAGE)

    @property
    @resilient_cached_webelement
    def previous_page_element(self):
        return self.web_element.find_element(*self.PREVIOUS_PAGE)

    @property
    @resilient_cached_webelement
    def first_page_element(self):
        return self.web_element.find_element(*self.FIRST_PAGE)

    @property
    @resilient_cached_webelement
    def last_page_element(self):
        return self.web_element.find_element(*self.LAST_PAGE)

    def get_pages(self) -> list[Page]:
        if not self._pages:
            self._pages = [
                Page(int(e.get_attribute("textContent") or ""), e)
                for e in self.web_element.find_elements(*self.PAGE)
            ]
        return self._pages

    def get_page(self, index: int) -> Page:
        return self.get_pages()[index]

    def get_active_page(self) -> Page | None:
        pages = self.get_pages()
        return next(
            (
                page
                for page in pages
                if PrimefacesSelenium.has_css_class(page.web_element, "ui-state-active")
            ),
            None,
        )
