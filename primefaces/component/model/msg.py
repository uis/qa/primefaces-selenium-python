import dataclasses

from primefaces.component.model.severity import Severity


@dataclasses.dataclass
class Msg:
    severity: Severity | None = None
    summary: str | None = None
    detail: str | None = None
