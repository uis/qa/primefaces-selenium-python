from abc import ABC, abstractmethod
from functools import cached_property

from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selocity import resilient_cached_webelement

from primefaces.component.base.component_utils import ComponentUtils
from primefaces.primefaces_selenium import PrimefacesSelenium


class SelectItem(ABC):
    def __init__(
        self,
        driver: WebDriver,
        web_element: WebElement,
        box_locator: tuple[str, str],
    ):
        self.driver = driver
        self.web_element = web_element
        self.box_locator = box_locator

    @property
    @resilient_cached_webelement
    def box_element(self) -> WebElement:
        return self.web_element.find_element(*self.box_locator)

    @property
    @resilient_cached_webelement
    def input_element(self) -> WebElement:
        return self.web_element.find_element(By.TAG_NAME, "input")

    @property
    @resilient_cached_webelement
    def label_element(self) -> WebElement:
        if _labels := self.driver.find_elements(
            by=By.CSS_SELECTOR,
            value=f"label[for='{self.input_element.get_attribute('id')}']",
        ):
            return _labels[0]
        else:
            parent_element = self.input_element
            while True:
                try:
                    # Move to the parent element
                    parent_element = parent_element.find_element(by=By.XPATH, value="..")

                    # Get the id attribute of the parent element
                    parent_id = parent_element.get_attribute("id")

                    # Check if the id is not an empty string
                    if parent_id:
                        break
                except NoSuchElementException:
                    # Handle case where there is no parent element
                    break
            return self.driver.find_element(
                By.CSS_SELECTOR,
                f"label[for='{parent_element.get_attribute('id')}']",
            )

    @property
    @abstractmethod
    def label(self) -> str:
        pass

    @property
    def value(self) -> str:
        return self.input_element.get_attribute("value")

    def is_selected(self) -> bool:
        return PrimefacesSelenium(self.driver).has_css_class(self.box_element, "ui-state-active")

    @abstractmethod
    def toggle(self):
        pass

    def select(self):
        if not self.is_selected():
            self.toggle()

    def deselect(self):
        if self.is_selected():
            self.toggle()


class RadioSelectItem(SelectItem):
    @cached_property
    def label(self) -> str:
        return self.label_element.text

    def toggle(self):
        if ComponentUtils(self.driver).is_ajax_script(
            self.input_element.get_attribute("onchange")
        ):
            PrimefacesSelenium(self.driver).guard_ajax(self.web_element.click)
        else:
            self.web_element.click()


class CheckboxSelectItem(SelectItem):
    @property
    def label(self) -> str:
        return self.label_element.get_attribute("textContent")

    def toggle(self):
        _input = self.web_element.find_element(
            by=By.XPATH, value=".//div[starts-with(@class, 'ui-chkbox')]"
        )
        if ComponentUtils(self.driver).is_ajax_script(_input.get_attribute("onchange")):
            PrimefacesSelenium(self.driver).guard_ajax(self.web_element.click)
        else:
            self.web_element.click()
