import time
from functools import cached_property

from selenium.common import NoSuchElementException
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selocity import resilient_cached_webelement

from primefaces.component.model.data_table.cell import Cell
from primefaces.component.select_one_menu import SelectOneMenu
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions
from primefaces.primefaces_selenium import PrimefacesSelenium


class HeaderCell(Cell):
    def __init__(
        self,
        web_driver: WebDriver,
        web_element: WebElement,
    ):
        super().__init__(web_element)
        self.web_driver = web_driver

    @property
    @resilient_cached_webelement
    def column_title_element(self) -> WebElement:
        """Gets the title element for this column."""
        return self.web_element.find_element(By.CLASS_NAME, "ui-column-title")

    @property
    @resilient_cached_webelement
    def column_filter_element(self) -> WebElement:
        """Gets the title element for this column."""
        return self.web_element.find_element(By.CLASS_NAME, "ui-column-filter")

    @cached_property
    def text(self) -> str:
        return self.column_title_element.get_attribute("textContent")

    def set_filter_value(
        self,
        filter_value: str,
        filter_event: str = "",
        filter_delay: int = 0,
        cfg: dict | None = None,
    ) -> None:
        ps = PrimefacesSelenium(self.web_driver)
        select_menu = self.web_element.find_elements(By.CLASS_NAME, "ui-selectonemenu")
        if select_menu:
            select_id = self.web_element.find_elements(By.CLASS_NAME, "ui-selectonemenu")[
                0
            ].get_attribute("id")
            if not select_id:
                raise NoSuchElementException(
                    "Could not find select 'ID' attribute in header cell."
                )
            SelectOneMenu(
                web_driver=self.web_driver, base_element_locator=(By.ID, select_id)
            ).select(filter_value)
            ps.wait_gui().until(PrimefacesExpectedConditions().animation_not_active())
            return

        if cfg:
            filter_event = cfg["filterEvent"]
            filter_delay = cfg["filterDelay"]
        try:
            column_filter = self.column_filter_element
        except NoSuchElementException:
            column_filter = self.web_element.find_element(By.TAG_NAME, "input")
        column_filter.clear()
        trigger_key = None
        filter_event = filter_event.lower()
        if filter_event in ["keyup", "keydown", "keypress", "input"]:
            if filter_delay == 0:
                column_filter = ps.guard_ajax(column_filter)
        elif filter_event == "enter":
            trigger_key = Keys.ENTER
        elif filter_event in {"change", "blur"}:
            trigger_key = Keys.TAB
        else:
            trigger_key = None

        if filter_value is not None:
            column_filter.send_keys(filter_value)
        else:
            column_filter.send_keys(Keys.BACKSPACE)

        if trigger_key is not None:
            ps.guard_ajax(column_filter).send_keys(trigger_key)
        elif filter_delay > 0:
            time.sleep(filter_delay * 2 / 1000)
            ps.wait_gui().until(PrimefacesExpectedConditions().animation_not_active())

    def __str__(self) -> str:
        return f"HeaderCell{{text={self.text}}}"
