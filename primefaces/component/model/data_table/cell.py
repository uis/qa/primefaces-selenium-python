from functools import cached_property

from selenium.webdriver.remote.webelement import WebElement


class Cell:
    def __init__(self, web_element: WebElement):
        self.web_element = web_element

    @cached_property
    def text(self) -> str:
        if self.web_element:
            text_content = self.web_element.get_attribute("textContent")
            if text_content is not None:
                return text_content
        return ""
