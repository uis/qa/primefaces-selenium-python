from selenium.webdriver.remote.webelement import WebElement

from primefaces.component.model.data_table.cell import Cell


class DataTableRow:
    def __init__(
        self,
        web_element: WebElement,
        cells: list[Cell],
    ):
        self.web_element = web_element
        self.cells = cells

    def get_cell(self, index: int):
        return self.cells[index]
