from selenium.webdriver.remote.webelement import WebElement

from primefaces.component.model.data_table.header_cell import HeaderCell


class Header:
    def __init__(self, web_element: WebElement, cells: list[HeaderCell]):
        self.web_element = web_element
        self.cells = cells

    def get_cell(self, index: int) -> HeaderCell:
        return self.cells[index]

    def get_cell_by_header(self, header_text: str) -> HeaderCell | None:
        return next(
            (
                cell
                for cell in self.cells
                if cell.column_title_element.get_attribute("textContent") == header_text
            ),
            None,
        )
