from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selocity import resilient_cached_webelement, resilient_cached_webelements

from primefaces.component.base.abstract_component import AbstractComponent
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions
from primefaces.primefaces_selenium import PrimefacesSelenium


class Carousel(AbstractComponent):
    HEADER = (By.CLASS_NAME, "ui-carousel-header")
    ITEMS = (By.XPATH, "//*[contains(@class, 'ui-carousel-items')]")
    FOOTER = (By.CLASS_NAME, "ui-carousel-footer")
    ITEM = (By.CLASS_NAME, "ui-carousel-item")
    PAGE_LINKS = (
        By.XPATH,
        "//*[contains(@class, 'ui-carousel-page-links') "
        "or contains(@class, 'ui-carousel-indicators')]",
    )
    PREV_BUTTON = (By.XPATH, ".//*[contains(@class, 'ui-carousel-prev')]")
    NEXT_BUTTON = (By.XPATH, ".//*[contains(@class, 'ui-carousel-next')]")

    @property
    @resilient_cached_webelement
    def header_element(self) -> WebElement:
        return self.root_element.find_element(*self.HEADER)

    @property
    @resilient_cached_webelement
    def footer_element(self) -> WebElement:
        return self.root_element.find_element(*self.FOOTER)

    @property
    @resilient_cached_webelement
    def items_content_element(self) -> WebElement:
        return self.root_element.find_element(*self.ITEMS)

    @property
    @resilient_cached_webelements
    def item_elements(self) -> list[WebElement]:
        return self.root_element.find_elements(*self.ITEM)

    @property
    @resilient_cached_webelements
    def page_elements(self) -> list[WebElement]:
        return self.root_element.find_element(*self.PAGE_LINKS).find_elements(
            By.CSS_SELECTOR, "a, li"
        )

    @property
    @resilient_cached_webelement
    def previous_button_element(self) -> WebElement:
        return self.root_element.find_element(*self.PREV_BUTTON)

    @property
    @resilient_cached_webelement
    def next_button_element(self) -> WebElement:
        return self.root_element.find_element(*self.NEXT_BUTTON)

    def get_item_by_index(self, index: int) -> WebElement:
        return self.item_elements[index]

    def go_to_previous_page(self) -> None:
        if not PrimefacesSelenium.has_css_class(self.previous_button_element, "ui-state-disabled"):
            self.previous_button_element.click()
            PrimefacesSelenium(self.web_driver).wait_gui().until(
                PrimefacesExpectedConditions().visible_and_animation_complete(
                    self.items_content_element
                )
            )

    def go_to_next_page(self) -> None:
        if not PrimefacesSelenium.has_css_class(self.next_button_element, "ui-state-disabled"):
            self.next_button_element.click()
            PrimefacesSelenium(self.web_driver).wait_gui().until(
                PrimefacesExpectedConditions().visible_and_animation_complete(
                    self.items_content_element
                )
            )
