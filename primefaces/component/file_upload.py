from functools import cached_property

from selenium.webdriver.common.by import By
from selocity import resilient_cached_webelement

from primefaces.component.base.abstract_input_component import AbstractInputComponent
from primefaces.internal.config_provider import ConfigProvider
from primefaces.internal.guard import Guard
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions
from primefaces.primefaces_selenium import PrimefacesSelenium


class FileUpload(AbstractInputComponent):
    @cached_property
    def is_auto_upload(self):
        return bool(self.widget_configuration.get("auto"))

    @cached_property
    def is_advanced_mode(self):
        mode = self.widget_configuration.get("mode")
        if mode is None:
            return False
        else:
            return mode.get("advanced")

    @property
    @resilient_cached_webelement
    def input_element(self):
        is_input_file = (
            self.root_element.tag_name == "input"
            and self.root_element.get_attribute("type") == "file"
        )
        return (
            self.root_element
            if is_input_file
            else self.root_element.find_element(value=f"{self.root_dom_id}_input")
        )

    def get_value(self):
        self.input_element.get_attribute("value")

    def get_widget_error_messages(self):
        return [
            e.get_attribute("textContent")
            for e in self.root_element.find_elements((By.CLASS_NAME, "ui-messages-error-summary"))
        ]

    def get_widget_values(self):
        return [
            e.get_attribute("textContent")
            for e in self.root_element.find_elements((By.CLASS_NAME, "ui-fileupload-filename"))
        ]

    def get_advanced_cancel_button(self, file_name: str | None = None):
        if file_name is None:
            return self.root_element.find_element(
                By.CSS_SELECTOR,
                ".ui-fileupload-buttonbar button.ui-fileupload-cancel",
            )
        for row in self.root_element.find_elements(
            By.CSS_SELECTOR, ".ui-fileupload-files .ui-fileupload-row"
        ):
            fn = row.find_element(By.CSS_SELECTOR, ".ui-fileupload-filename")
            if file_name in fn.get_attribute("textContent"):
                return row.find_element(By.CSS_SELECTOR, "button.ui-fileupload-cancel")

    def get_widget_value(self):
        return self.root_element.find_element(
            By.CLASS_NAME, "ui-fileupload-filename"
        ).get_attribute("textContent")

    def set_value(self, value, use_guard: bool = True):
        if use_guard and self.is_auto_upload and self.is_advanced_mode:
            Guard(self.web_driver).custom(
                self.input_element.send_keys,
                0.2,
                ConfigProvider.TIMEOUT_FILE_UPLOAD,
                PrimefacesExpectedConditions().script(
                    f"return {self.widget_id}.files.length === 0;"
                ),
                str(value),
            )
        else:
            PrimefacesSelenium(self.web_driver).guard_ajax(
                self.input_element.send_keys, str(value)
            )
