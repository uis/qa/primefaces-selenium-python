from functools import cached_property

from selenium.webdriver.common.by import By
from selocity import resilient_cached_webelement

from primefaces.component.base.abstract_component import AbstractComponent
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions
from primefaces.primefaces_selenium import PrimefacesSelenium


class Dialog(AbstractComponent):
    CONTENT = By.CLASS_NAME, "ui-dialog-content"
    TITLE = By.CLASS_NAME, "ui-dialog-title"

    @property
    @resilient_cached_webelement
    def content_element(self):
        return self.root_element.find_element(self.CONTENT)

    @cached_property
    def get_title(self):
        return self.root_element.find_element(self.TITLE).get_attribute("textContent")

    def is_visible(self):
        return PrimefacesSelenium(self.web_driver).execute_script(
            f"return {self.widget_id}.isVisible();"
        )

    def show(self):
        if self.is_enabled() and not self.is_displayed():
            ps = PrimefacesSelenium(self.web_driver)
            ps.execute_script(f"{self.widget_id}.show();")
            ps.wait_gui().until(
                PrimefacesExpectedConditions().visible_and_animation_complete(self.root_element)
            )

    def hide(self):
        if self.is_enabled() and self.is_displayed():
            ps = PrimefacesSelenium(self.web_driver)
            ps.execute_script(f"{self.widget_id}.hide();")
            ps.wait_gui().until(
                PrimefacesExpectedConditions().invisible_and_animation_complete(self.root_element)
            )
