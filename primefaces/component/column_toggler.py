from functools import cached_property

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selocity import resilient_cached_webelement, resilient_cached_webelements

from primefaces.component.base.abstract_component import AbstractComponent
from primefaces.primefaces_selenium import PrimefacesSelenium


class ColumnToggler(AbstractComponent):
    @cached_property
    def labels(self):
        return [_box.get_attribute("textContent") for _box in self.item_elements]

    @property
    @resilient_cached_webelement
    def menu_element(self) -> WebElement:
        toggle_button_name = self.root_element.get_attribute("name")
        base_id = toggle_button_name.replace(":toggler", "")
        return self.web_driver.find_element(
            By.XPATH,
            f"//*[starts-with(@id, '{base_id}') and contains(@class, 'ui-columntoggler')][1]",
        )

    @property
    @resilient_cached_webelements
    def item_elements(self):
        return self.menu_element.find_elements(By.TAG_NAME, "li")

    @property
    @resilient_cached_webelements
    def checkbox_elements(self) -> list[WebElement]:
        checkboxes = []

        def _get_checkbox(_container):
            divs = _container.find_elements(by=By.TAG_NAME, value="div")
            for div in divs:
                if PrimefacesSelenium(self.web_driver).element_has_css_classes(div, "ui-chkbox"):
                    checkboxes.append(div)
                    return

        for item in self.item_elements:
            _get_checkbox(item)
        return checkboxes

    def show_toggle_menu(self):
        if self.menu_element.is_displayed():
            return
        self.root_element.click()
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            expected_conditions.visibility_of(self.menu_element)
        )

    def hide_toggle_menu(self):
        if not self.menu_element.is_displayed():
            return
        self.root_element.click()
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            expected_conditions.invisibility_of_element(self.menu_element)
        )

    def toggle(self, *indices):
        for index in indices:
            _checkbox = self.checkbox_elements[index]
            _input = _checkbox.find_element(
                by=By.XPATH, value=".//div[starts-with(@class, 'ui-chkbox')]"
            )
            if self.is_ajaxified("onchange", _input):
                PrimefacesSelenium(self.web_driver).guard_ajax(_checkbox.click)
            else:
                _checkbox.click()

    def toggle_all(self):
        for index in range(len(self.checkbox_elements)):
            self.toggle(index)

    def deselect_all(self, ignore_indices: list | None = None):
        ignore_indices = ignore_indices or []
        for index in range(len(self.checkbox_elements)):
            if index not in ignore_indices and self.is_selected_by_index(index):
                self.toggle(index)

    def select_all(self):
        for index in range(len(self.checkbox_elements)):
            if not self.is_selected_by_index(index):
                self.toggle(index)

    def select(self, *indices):
        self.deselect_all([*indices])
        for index in indices:
            if not self.is_selected_by_index(index):
                self.toggle(index)

    def deselect(self, *indices):
        for index in indices:
            if self.is_selected_by_index(index):
                self.toggle(index)

    def get_label(self, index):
        return self.labels[index]

    def is_selected_by_index(self, index: int) -> bool:
        checkbox = self.checkbox_elements[index]
        _box = checkbox.find_element(by=By.CLASS_NAME, value="ui-chkbox-box")
        return PrimefacesSelenium(self.web_driver).has_css_class(_box, "ui-state-active")

    def get_index_for_label(self, label: str, strip_whitespace: bool = True):
        for index, _label in enumerate(self.labels):
            if strip_whitespace:
                _label = _label.strip()
            if _label == label:
                return index

        class LabelNotFound(BaseException):
            pass

        raise LabelNotFound(f"No label found matching text: {label}")
