from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions

from ..primefaces_selenium import PrimefacesSelenium
from .base.abstract_component import AbstractComponent
from .model.msg import Msg
from .model.severity import Severity


class Messages(AbstractComponent):
    def is_empty(self) -> bool:
        return not self.is_displayed() or not self.get_all_messages()

    def get_all_messages(self) -> list[Msg]:
        if not self.is_displayed():
            return []

        result = []
        messages_severities = self.root_element.find_elements(By.TAG_NAME, "div")
        for message_severity in messages_severities:
            severity_class = message_severity.get_attribute("class")
            severity = Severity.to_severity(severity_class)

            for message in message_severity.find_elements(By.CSS_SELECTOR, "li"):
                summary_class = f"ui-messages-{severity}-summary"
                summary = message.find_element(By.CLASS_NAME, summary_class).text
                msg = Msg(severity=severity, summary=summary)
                try:
                    detail_class = f"ui-messages-{severity}-detail"
                    msg.detail = message.find_element(By.CLASS_NAME, detail_class).text
                except NoSuchElementException:
                    # Ignore, it's optional
                    pass
                result.append(msg)

        return result

    def get_message(self, index: int) -> Msg | None:
        all_messages = self.get_all_messages()
        return all_messages[index] if index < len(all_messages) else None

    def get_messages_by_severity(self, severity: Severity) -> list[Msg]:
        return [message for message in self.get_all_messages() if message.severity == severity]

    def get_all_summaries(self) -> list[str | None]:
        return [message.summary for message in self.get_all_messages()]

    def wait_for_messages(self, severity: Severity | None = None, timeout: int = 10) -> list[Msg]:
        wait = PrimefacesSelenium(self.web_driver).wait_gui()
        try:
            if severity is not None:
                severity_class = f"ui-messages-{severity}"
                wait.until(
                    expected_conditions.presence_of_element_located(
                        (By.CLASS_NAME, severity_class)
                    )
                )
                return [msg for msg in self.get_all_messages() if msg.severity == severity]
            else:
                wait.until(
                    expected_conditions.presence_of_element_located(
                        (By.CSS_SELECTOR, "div.ui-messages")
                    )
                )
                return self.get_all_messages()
        except TimeoutException:
            severity_name = severity.name if severity else "any"
            raise TimeoutException(
                f"No message with severity {severity_name} appeared within {timeout} seconds."
            )
