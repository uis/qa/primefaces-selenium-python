from selenium.webdriver.common.by import By

from primefaces.component.base.abstract_table import AbstractTable
from primefaces.component.model.data_table.cell import Cell
from primefaces.component.model.data_table.data_table_row import DataTableRow
from primefaces.primefaces_selenium import PrimefacesSelenium


class DataTable(AbstractTable):
    def get_rows(self) -> list[DataTableRow]:
        rows = []
        for e in self.rows_elements:
            if not PrimefacesSelenium(self.web_driver).has_css_class(
                e, "ui-datatable-empty-message"
            ):
                # StaleElementException seems to be thrown by the previous method call
                _cells = e.find_elements(By.TAG_NAME, "td")
                cells = [Cell(e2) for e2 in _cells]
                rows.append(DataTableRow(e, cells))
        return rows

    def get_all_rows(self) -> list[DataTableRow]:
        total_pages = len(self.paginator.get_pages())
        if total_pages == 1:
            return self.get_rows()
        rows = []
        self.first_page()
        for i in range(total_pages):
            rows.extend(self.get_rows())
            self.next_page()
        self.first_page()
        return rows

    def get_row(self, index: int) -> DataTableRow:
        return self.get_rows()[index]
