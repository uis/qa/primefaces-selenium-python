from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selocity import resilient_cached_webelements

from primefaces.component.base.abstract_component import AbstractComponent
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions
from primefaces.primefaces_selenium import PrimefacesSelenium


class BlockUI(AbstractComponent):
    @property
    @resilient_cached_webelements
    def overlay_elements(self) -> list[WebElement]:
        return self.root_element.find_elements(By.CLASS_NAME, "ui-blockui")

    @resilient_cached_webelements
    def content_elements(self) -> list[WebElement]:
        return self.root_element.find_elements(By.CLASS_NAME, "ui-blockui-content")

    def is_blocking(self):
        return PrimefacesSelenium(self.web_driver).execute_script(
            f"return {self.widget_id}.isBlocking();"
        )

    def has_content(self):
        return PrimefacesSelenium(self.web_driver).execute_script(
            f"return {self.widget_id}.hasContent();"
        )

    def show(self):
        PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.show();")
        if overlays := self.overlay_elements:
            PrimefacesSelenium(self.web_driver).wait_gui().until(
                PrimefacesExpectedConditions().visible_and_animation_complete(overlays[0])
            )

    def hide(self):
        PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.hide();")
        if overlays := self.overlay_elements:
            PrimefacesSelenium(self.web_driver).wait_gui().until(
                PrimefacesExpectedConditions().invisible_and_animation_complete(overlays[0])
            )
