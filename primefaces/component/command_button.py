from selenium.webdriver.support import expected_conditions

from primefaces.component.button import Button
from primefaces.primefaces_selenium import PrimefacesSelenium


class CommandButton(Button):
    def click_unguarded(self):
        """
        For some scenarios with ajax="false", like in a download,
        you may not want to guard the click.
        """
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            expected_conditions.element_to_be_clickable(self.root_element)
        )
        self.root_element.click()
