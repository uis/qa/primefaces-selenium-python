from selenium.webdriver.common.by import By
from selocity import resilient_cached_webelement

from primefaces.component.input_text import InputText
from primefaces.primefaces_selenium import PrimefacesSelenium


class InputNumber(InputText):
    @property
    @resilient_cached_webelement
    def hidden_input_element(self):
        return self.root_element.find_element(
            By.XPATH,
            f"//*[@id='{self.root_element.get_attribute('id')}_hinput']",
        )

    @property
    @resilient_cached_webelement
    def input_element(self):
        return self.web_driver.find_element(
            By.XPATH,
            f"//*[@id='{self.root_element.get_attribute('id')}_input']",
        )

    def set_value(self, value):
        if value is None:
            value = '""'
        PrimefacesSelenium(self.web_driver).execute_script(self.widget_id + f".setValue({value})")
        ajaxified = self.is_on_change_ajaxified()
        if ajaxified:  # not sure whether this if is correct
            PrimefacesSelenium(self.web_driver).guard_ajax_for_script(
                self.widget_id + ".input.trigger('change')"
            )
        else:
            PrimefacesSelenium(self.web_driver).execute_script(
                self.widget_id + ".input.trigger('change')"
            )

    def get_value_to_render(self):
        return float(
            PrimefacesSelenium(self.web_driver).execute_script(
                f"return {self.widget_id}.valueToRender;"
            )
        )

    def get_widget_value(self):
        return PrimefacesSelenium(self.web_driver).execute_script(
            f"return {self.widget_id}.getValue();"
        )
