import json
from functools import cached_property

from selenium.webdriver.remote.webelement import WebElement

from primefaces.abstract_primefaces_page_fragment import AbstractPrimefacesPageFragment
from primefaces.primefaces_selenium import PrimefacesSelenium

from .component_utils import ComponentUtils


class AbstractComponent(AbstractPrimefacesPageFragment):
    @cached_property
    def command_key(self):
        return PrimefacesSelenium(self.web_driver).command_key

    # CSP script using jQuery events to see if an event is AJAXified.
    CSP_SCRIPT = "return PrimeFaces.csp.hasRegisteredAjaxifiedEvent('%s', '%s');"

    @cached_property
    def widget_id(self):
        """
        Gets the widget by component id JS function.
        :returns The JS script: str
        """
        return ComponentUtils(self.web_driver).get_widget_by_id_script(self.root_dom_id)

    @cached_property
    def widget_configuration_string(self) -> str | None:
        """
        Gets the current widget's configuration e.g. widget.cfg as a String.
        :returns the String representation of the widget configuration
        """
        return ComponentUtils(self.web_driver).get_widget_configuration(self.root_element)

    @cached_property
    def widget_configuration(self) -> dict:
        """
        Gets the current widget's configuration e.g. widget.cfg as a dict.
        :returns dict: the dict representing the config, useful for assertions
        """
        cfg = self.widget_configuration_string
        return json.loads(cfg) if cfg else {}

    def is_ajaxified(self, event: str, element: WebElement | None = None) -> bool:
        """
        Is the event for the root-element AJAXified?
        :param event: Event with the 'on' prefix, such as 'onclick' or 'onblur': str
        :param element: Element for which to do the check.
                                (It may be a child element of a complex component)
                                If no element is passed it defaults to getRoot(): WebElement
        :returns true if using AJAX, false if not: bool
        """
        element = element or self.root_element
        if ComponentUtils(self.web_driver).is_ajax_script(element.get_attribute(event)):
            return True

        # now check for CSP events
        # id_ = element.get_attribute("id")
        # csp_script = self.CSP_SCRIPT.format(id_, event)
        # Doesn't work with current PF version
        # return PrimefacesSelenium(self.web_driver).execute_script(csp_script)
        return False

    def destroy(self):
        """
        Destroys the widget
        :return:
        """
        PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.destroy();")
