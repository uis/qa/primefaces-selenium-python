from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from primefaces.primefaces_selenium import PrimefacesSelenium

from .abstract_component import AbstractComponent
from .component_utils import ComponentUtils


class AbstractInputComponent(AbstractComponent):
    @property
    def input_element(self) -> WebElement:
        return self.root_element

    def is_enabled(self):
        return self.input_element.is_enabled() and not PrimefacesSelenium(
            self.web_driver
        ).has_css_class(self.root_element, "ui-state-disabled")

    def is_on_change_ajaxified(self):
        return self.is_ajaxified(element=self.input_element, event="onchange") or ComponentUtils(
            self.web_driver
        ).has_ajax_behavior(self.root_element, "change")

    def get_assigned_label(self):
        return self.web_driver.find_element(
            By.CSS_SELECTOR,
            f"label[for='{self.input_element.get_attribute('id')}']",
        )

    def get_assigned_label_text(self):
        return self.get_assigned_label().get_attribute("textContent")

    def copy_to_clipboard(self):
        self.input_element.send_keys(self.command_key, "a")
        self.input_element.send_keys(self.command_key, "c")
        return self.input_element.get_attribute("value")

    def paste_from_clipboard(self):
        element = self.input_element
        element.send_keys(self.command_key, "a")
        element.send_keys(self.command_key, "v")
        return element.get_attribute("value")

    def select_all_text(self):
        self.input_element.send_keys(self.command_key, "a")

    def clear(self):
        PrimefacesSelenium(self.web_driver).clear_input(self.input_element, False)

    def enable(self):
        PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.enable();")

    def disable(self):
        PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.disable();")
