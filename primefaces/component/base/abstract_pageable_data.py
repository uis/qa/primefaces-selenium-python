import abc

from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selocity import resilient_cached_webelement

from primefaces.component.model.data.page import Page
from primefaces.component.model.data.paginator import Paginator
from primefaces.primefaces_selenium import PrimefacesSelenium

from .abstract_component import AbstractComponent


class AbstractPageableData(AbstractComponent):
    PAGINATOR = (By.CLASS_NAME, "ui-paginator")

    @property
    @resilient_cached_webelement
    def paginator_element(self):
        return self.root_element.find_element(*self.PAGINATOR)

    @abc.abstractmethod
    def rows_elements(self):
        pass

    @property
    def paginator(self) -> Paginator:
        return Paginator(self.paginator_element)

    def select_page(self, page: Page) -> None:
        active_page = self.paginator.get_active_page()

        if active_page is None or active_page.number != page.number:
            PrimefacesSelenium(self.web_driver).guard_ajax(page.web_element.click)

    def select_page_by_index(self, index: int) -> None:
        for page in self.paginator.get_pages():
            if page.number == index:
                self.select_page(page)
                return

    def select_rows_per_page(self, rows) -> None:
        rppDD = self.web_driver.find_element(By.NAME, f"{self.root_dom_id}_rppDD")
        Select(rppDD).select_by_value(f"{rows}")
        # guard_ajax does not work in this case so ignore for now
        # PrimefacesSelenium(self.web_driver).guard_ajax(Select(rppDD).select_by_value(f"{rows}"))

    def next_page(self) -> None:
        next_page_elem = self.paginator.next_page_element
        if not PrimefacesSelenium.has_css_class(next_page_elem, "ui-state-disabled"):
            PrimefacesSelenium(self.web_driver).guard_ajax(next_page_elem.click)

    def previous_page(self):
        prev_page_elem = self.paginator.previous_page_element
        if not PrimefacesSelenium.has_css_class(prev_page_elem, "ui-state-disabled"):
            PrimefacesSelenium(self.web_driver).guard_ajax(prev_page_elem.click)

    def first_page(self):
        first_page_elem = self.paginator.first_page_element
        if not PrimefacesSelenium.has_css_class(first_page_elem, "ui-state-disabled"):
            PrimefacesSelenium(self.web_driver).guard_ajax(first_page_elem.click)

    def last_page(self):
        last_page_elem = self.paginator.last_page_element
        if not PrimefacesSelenium.has_css_class(last_page_elem, "ui-state-disabled"):
            PrimefacesSelenium(self.web_driver).guard_ajax(last_page_elem.click)
