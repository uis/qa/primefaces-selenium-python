from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selocity import resilient_cached_webelement

from .abstract_component import AbstractComponent


class AbstractMessage(AbstractComponent):
    ICON_ERROR_LOCATOR = (By.CSS_SELECTOR, ".ui-message-error-icon")
    ICON_INFO_LOCATOR = (By.CSS_SELECTOR, ".ui-message-info-icon")
    ICON_WARN_LOCATOR = (By.CSS_SELECTOR, ".ui-message-warn-icon")
    ICON_FATAL_LOCATOR = (By.CSS_SELECTOR, ".ui-message-fatal-icon")
    SUMMARY_ERROR_LOCATOR = (By.CSS_SELECTOR, ".ui-message-error-summary")
    SUMMARY_INFO_LOCATOR = (By.CSS_SELECTOR, ".ui-message-info-summary")
    SUMMARY_WARN_LOCATOR = (By.CSS_SELECTOR, ".ui-message-warn-summary")
    SUMMARY_FATAL_LOCATOR = (By.CSS_SELECTOR, ".ui-message-fatal-summary")
    DETAIL_ERROR_LOCATOR = (By.CSS_SELECTOR, ".ui-message-error-detail")
    DETAIL_INFO_LOCATOR = (By.CSS_SELECTOR, ".ui-message-info-detail")
    DETAIL_WARN_LOCATOR = (By.CSS_SELECTOR, ".ui-message-warn-detail")
    DETAIL_FATAL_LOCATOR = (By.CSS_SELECTOR, ".ui-message-fatal-detail")

    @property
    @resilient_cached_webelement
    def error_icon_element(self) -> WebElement:
        return self.root_element.find_element(*self.ICON_ERROR_LOCATOR)

    @property
    @resilient_cached_webelement
    def info_icon_element(self) -> WebElement:
        return self.root_element.find_element(*self.ICON_INFO_LOCATOR)

    @property
    @resilient_cached_webelement
    def warning_icon_element(self) -> WebElement:
        return self.root_element.find_element(*self.ICON_WARN_LOCATOR)

    @property
    @resilient_cached_webelement
    def fatal_icon_element(self) -> WebElement:
        return self.root_element.find_element(*self.ICON_FATAL_LOCATOR)

    @property
    @resilient_cached_webelement
    def error_summary_element(self) -> WebElement:
        return self.root_element.find_element(*self.SUMMARY_ERROR_LOCATOR)

    @property
    @resilient_cached_webelement
    def info_summary_element(self) -> WebElement:
        return self.root_element.find_element(*self.SUMMARY_INFO_LOCATOR)

    @property
    @resilient_cached_webelement
    def warning_summary_element(self) -> WebElement:
        return self.root_element.find_element(*self.SUMMARY_WARN_LOCATOR)

    @property
    @resilient_cached_webelement
    def fatal_summary_element(self) -> WebElement:
        return self.root_element.find_element(*self.SUMMARY_FATAL_LOCATOR)

    @property
    @resilient_cached_webelement
    def error_detail_element(self) -> WebElement:
        return self.root_element.find_element(*self.DETAIL_ERROR_LOCATOR)

    @property
    @resilient_cached_webelement
    def info_detail_element(self) -> WebElement:
        return self.root_element.find_element(*self.DETAIL_INFO_LOCATOR)

    @property
    @resilient_cached_webelement
    def warning_detail_element(self) -> WebElement:
        return self.root_element.find_element(*self.DETAIL_WARN_LOCATOR)

    @property
    @resilient_cached_webelement
    def fatal_detail_element(self) -> WebElement:
        return self.root_element.find_element(*self.DETAIL_FATAL_LOCATOR)
