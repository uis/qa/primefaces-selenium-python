from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.event_firing_webdriver import EventFiringWebDriver


class ComponentUtils:
    def __init__(self, web_driver: WebDriver | EventFiringWebDriver):
        self.web_driver = web_driver

    def has_ajax_behavior(self, element: WebElement, b: str) -> bool:
        if not self.has_behavior(element, b):
            return False

        _id = element.get_attribute("id")
        result = self.web_driver.execute_script(
            f"""
            var widget = {self.get_widget_by_id_script(_id)};
            var cBehavior = '';
            if (widget && typeof widget.getBehavior === 'function') {{
                cBehavior = widget.getBehavior('{b}')?widget.getBehavior('{b}').toString():'';
            }}
            return cBehavior;"""
        )
        return self.is_ajax_script(result)

    def has_behavior(self, element: WebElement, b: str) -> bool:
        if not self.is_widget(element):
            return False

        _id = element.get_attribute("id")
        return bool(
            self.web_driver.execute_script(
                f"""
            var widget = {self.get_widget_by_id_script(_id)};
            var changeBehavior = false;
            if (widget && typeof widget.hasBehavior === 'function') {{
                changeBehavior = widget.hasBehavior('{b}') ? widget.hasBehavior('{b}') : false;
            }}
            return changeBehavior;
        """
            )
        )

    def is_widget(self, element: WebElement):
        if _id := element.get_attribute("id"):
            return self.web_driver.execute_script(
                f"return {self.get_widget_by_id_script(_id)} != null;"
            )
        else:
            return False

    @staticmethod
    def is_ajax_script(script: str | None) -> bool:
        if script is None:
            return False
        ajax_strings = [
            "PrimeFaces.ab(",
            "pf.ab(",
            "mojarra.ab(",
            "myfaces.ab(",
            "jsf.ajax.request",
            "faces.ajax.request",
        ]
        return any(substring in script for substring in ajax_strings)

    def get_widget_configuration(self, element: WebElement) -> str:
        _id = element.get_attribute("id")
        return self.web_driver.execute_script(
            f"""
            return JSON.stringify({self.get_widget_by_id_script(_id)}.cfg, function(key, value) {{
            if (typeof value === 'function') {{
                return value.toString();
              }} else if (value && value.constructor && value.constructor.name === 'RegExp') {{
                return value.toString();
              }} else {{
                return value;
              }}
            }});
            """
        )

    @staticmethod
    def get_widget_by_id_script(id_) -> str:
        return f"PrimeFaces.getWidgetById('{id_}')"
