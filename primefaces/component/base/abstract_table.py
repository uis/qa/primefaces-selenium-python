import abc
import json

from selenium.common import NoSuchElementException
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selocity import resilient_cached_webelement, resilient_cached_webelements

from primefaces.component.model.data_table.header import Header
from primefaces.component.model.data_table.header_cell import HeaderCell
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions
from primefaces.primefaces_selenium import PrimefacesSelenium

from .abstract_input_component import AbstractInputComponent
from .abstract_pageable_data import AbstractPageableData
from .component_utils import ComponentUtils


class _GlobalFilter(AbstractInputComponent):
    @property
    @resilient_cached_webelement
    def input_element(self) -> WebElement:
        return self.get_by_partial_id_from_base_element("globalFilter")

    def get_value(self):
        return PrimefacesSelenium(self.web_driver).execute_script(
            "return arguments[0].value;", self.input_element
        )

    def set_value(self, value: str):
        self.input_element.send_keys(value)
        if self.is_on_change_ajaxified():
            PrimefacesSelenium(self.web_driver).guard_ajax(self.input_element.send_keys, Keys.TAB)
        else:
            self.input_element.send_keys(Keys.TAB)

    def clear(self):
        PrimefacesSelenium(self.web_driver).clear_input(
            self.input_element, self.is_on_change_ajaxified()
        )


class AbstractTable(AbstractPageableData):
    EMPTY_LOCATOR = (By.CSS_SELECTOR, ".ui-datatable-empty-message")

    @property
    @resilient_cached_webelement
    def header_element(self) -> WebElement:
        return self.root_element.find_element(By.TAG_NAME, "table").find_element(
            by=By.TAG_NAME, value="thead"
        )

    @property
    @resilient_cached_webelement
    def body_element(self):
        return self.root_element.find_element(By.TAG_NAME, "tbody")

    @property
    @resilient_cached_webelements
    def rows_elements(self) -> list[WebElement]:
        return self.body_element.find_elements(By.TAG_NAME, "tr")

    @property
    @resilient_cached_webelement
    def select_all_checkbox_element(self) -> WebElement:
        return self.get_header().get_cell(0).web_element

    @abc.abstractmethod
    def get_row(self, index: int):
        pass

    def get_cell(self, row_index: int, column_index: int):
        return self.get_row(row_index).get_cell(column_index)

    def is_table_empty(self):
        return bool(self.root_element.find_elements(*self.EMPTY_LOCATOR))

    def get_column_index_by_header_label(self, header_label: str):
        _cells: list[HeaderCell] = self.get_header().cells
        return next(
            (
                index
                for index, _header_cell in enumerate(_cells)
                if _header_cell.text == header_label
            ),
            -1,
        )

    def get_header(self):
        cells = [
            HeaderCell(self.web_driver, e)
            for e in self.header_element.find_elements(by=By.TAG_NAME, value="th")
        ]
        return Header(self.header_element, cells)

    def get_footer(self) -> WebElement | None:
        try:
            return self.get_by_partial_id_from_root("footer")
        except NoSuchElementException:
            return None

    def sort(self, header_text: str | None = None, index: int | None = None):
        """Sorts the column found by its header text. It toggles to the next sort direction."""
        if header_text is None and index is None:
            raise ValueError("Either `header_text` or `index` must be set!")
        cell = (
            self.get_header().get_cell_by_header(header_text)
            if header_text
            else self.get_header().get_cell(index)
        )
        if cell:
            PrimefacesSelenium(self.web_driver).guard_ajax(
                cell.web_element.find_element(By.CLASS_NAME, "ui-sortable-column-icon").click
            )
        else:

            class HeaderCellNotFound(BaseException):
                pass

            raise HeaderCellNotFound(f"Header Cell '{header_text or index}' not found.")

    def filter(
        self,
        filter_value: str | None,
        cell: HeaderCell | None = None,
        cell_index: int | None = None,
        header_text: str | None = None,
    ):
        if cell is None:
            header = self.get_header()
            cell = (
                header.get_cell_by_header(header_text)
                if header_text
                else header.get_cell(cell_index)
            )
        config = self.widget_configuration_string or "{}"
        cell.set_filter_value(filter_value or "", cfg=json.loads(config))

    def remove_filter(self, cell_index: int | None = None, header_text: str | None = None):
        self.filter(None, cell_index=cell_index, header_text=header_text)

    def toggle_select_all_checkbox(self):
        if ComponentUtils(self.web_driver).has_behavior(
            self.root_element, "rowSelect"
        ) or ComponentUtils(self.web_driver).has_behavior(self.root_element, "rowUnselect"):
            PrimefacesSelenium(self.web_driver).guard_ajax(self.select_all_checkbox_element.click)
        else:
            self.select_all_checkbox_element.click()

    def _get_global_filter(self) -> _GlobalFilter:
        return _GlobalFilter(self.web_driver, self.base_element_locator)

    def global_filter_search(self, search_term: str) -> None:
        if self._get_global_filter().get_value():
            self.clear_global_filter()
        if self.get_footer() is not None:
            _wait_element = self.get_footer()
        elif self.is_table_empty():
            _wait_element = self.root_element.find_element(*self.EMPTY_LOCATOR)
        else:
            _wait_element = self.rows_elements[0]
        if _wait_element is None:
            raise NoSuchElementException(
                "Couldn't find an element to wait for the table to refresh!"
            )
        self._get_global_filter().set_value(search_term)
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            PrimefacesExpectedConditions.staleness_of(_wait_element)
        )

    def clear_global_filter(self):
        if self.get_footer() is not None:
            _wait_element = self.get_footer()
        elif self.is_table_empty():
            _wait_element = self.root_element.find_element(*self.EMPTY_LOCATOR)
        else:
            _wait_element = self.rows_elements[0]
        if _wait_element is None:
            raise NoSuchElementException(
                "Couldn't find an element to wait for the table to refresh!"
            )
        self._get_global_filter().clear()
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            PrimefacesExpectedConditions.staleness_of(_wait_element)
        )
