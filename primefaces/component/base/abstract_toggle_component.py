from selenium.webdriver.support import expected_conditions
from selocity import resilient_cached_webelement

from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions
from primefaces.primefaces_selenium import PrimefacesSelenium

from .abstract_input_component import AbstractInputComponent
from .component_utils import ComponentUtils


class AbstractToggleComponent(AbstractInputComponent):
    @property
    @resilient_cached_webelement
    def input_element(self):
        return self.get_by_partial_id_from_base_element("_input")

    def get_value(self):
        """Gets the value of the toggle component."""
        return self.input_element.get_attribute("checked") is not None

    def is_on_change_ajaxified(self):
        return self.is_ajaxified("onchange", self.input_element) or ComponentUtils(
            self.web_driver
        ).has_ajax_behavior(self.root_element, "change")

    def click(self):
        ps = PrimefacesSelenium(self.web_driver)
        ps.wait_gui().until(
            PrimefacesExpectedConditions().visible_and_animation_complete(self.root_element)
        )
        ps.wait_gui().until(expected_conditions.element_to_be_clickable(self.root_element))
        if self.is_on_change_ajaxified():
            ps.guard_ajax(self.root_element.click)
        else:
            self.root_element.click()

    def toggle(self):
        """Turns this switch in case it is off, or turns of off in case it is on."""
        pf = PrimefacesSelenium(self.web_driver)
        if self.is_on_change_ajaxified():
            pf.execute_script_ajaxified(
                f"{self.widget_id}.toggle();",
            )
        else:
            pf.execute_script(
                f"{self.widget_id}.toggle();",
            )

    def check(self):
        """Turns this switch on if it is not already turned on."""
        ps = PrimefacesSelenium(self.web_driver)
        script = f"{self.widget_id}.check();"
        if self.is_on_change_ajaxified():
            ps.execute_script_ajaxified(script)
        else:
            ps.execute_script(script)

    def uncheck(self):
        """Turns this switch off if it is not already turned off."""
        ps = PrimefacesSelenium(self.web_driver)
        script = f"{self.widget_id}.uncheck();"
        if self.is_on_change_ajaxified():
            ps.execute_script_ajaxified(script)
        else:
            ps.execute_script(script)

    def set_value(self, value: bool):
        """Set the value of the toggle component."""
        if self.get_value() != value:
            self.click()

    def is_selected(self):
        return self.get_value()
