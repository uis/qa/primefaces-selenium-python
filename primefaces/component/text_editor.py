from selenium.webdriver.common.by import By
from selocity import resilient_cached_webelement

from primefaces.component.input_text import InputText
from primefaces.primefaces_selenium import PrimefacesSelenium


class TextEditor(InputText):
    @property
    @resilient_cached_webelement
    def input_element(self):
        return self.web_driver.find_element(
            By.XPATH,
            f"//*[@id='{self.root_dom_id}_input']",
        )

    @property
    @resilient_cached_webelement
    def editor_element(self):
        return self.web_driver.find_element(
            By.XPATH,
            f"//*[@id='{self.root_dom_id}_editor']",
        )

    @property
    @resilient_cached_webelement
    def toolbar_element(self):
        return self.web_driver.find_element(
            By.XPATH,
            f"//*[@id='{self.root_dom_id}_toolbar']",
        )

    def get_editor_value(self):
        return PrimefacesSelenium(self.web_driver).execute_script(
            f"return {self.widget_id}.getEditorValue();"
        )

    def set_value(self, value):
        # if value is None:
        #     value = '""'
        # PrimefacesSelenium(self.web_driver).execute_script(
        #     self.get_widget_by_id_script() + f".setValue('{value}');"
        # )

        # workaround since setValue does not work on textEditor at the moment
        self.clear()
        if value:
            self.web_driver.execute_script(
                "arguments[0].innerHTML = arguments[1];",
                self.editor_element.find_element(By.CLASS_NAME, "ql-editor"),
                value,
            )

    def clear(self):
        PrimefacesSelenium(self.web_driver).execute_script(self.widget_id + ".clear();")
