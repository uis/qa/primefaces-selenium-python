from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selocity import resilient_cached_webelements

from primefaces.component.base.abstract_component import AbstractComponent
from primefaces.component.model.select_item import RadioSelectItem
from primefaces.primefaces_selenium import PrimefacesSelenium


class SelectOneRadio(AbstractComponent):
    """
    Component wrapper for the PrimeFaces {@code p:selectOneRadio}.
    """

    @property
    @resilient_cached_webelements
    def radio_button_elements(self) -> list[WebElement]:
        return self.root_element.find_elements(By.CSS_SELECTOR, ".ui-radiobutton")

    def is_read_only(self):
        return PrimefacesSelenium(self.web_driver).execute_script(
            f"return {self.widget_id}.cfg.readonly"
        )

    def enable_option(self, index: int):
        """Enables a given radio button option of this widget."""
        return PrimefacesSelenium(self.web_driver).execute_script(
            f"{self.widget_id}.enable({index});"
        )

    def disable_option(self, index: int):
        """Disables a given radio button option of this widget."""
        return PrimefacesSelenium(self.web_driver).execute_script(
            f"{self.widget_id}.disable({index});"
        )

    def enable(self):
        """Enables the entire component"""
        return PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.enable();")

    def disable(self):
        """Disables the entire component"""
        return PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.disable();")

    def get_items(self) -> list[RadioSelectItem]:
        box_locator = (By.CLASS_NAME, "ui-radiobutton-box")
        return [
            RadioSelectItem(
                driver=self.web_driver,
                web_element=radio_button,
                box_locator=box_locator,
            )
            for radio_button in self.radio_button_elements
            if radio_button.find_elements(*box_locator)
        ]

    def get_item_at_index(self, index: int) -> RadioSelectItem | None:
        """Returns the RadioSelectItem at the given index efficiently, or None if out of range."""
        box_locator = (By.CLASS_NAME, "ui-radiobutton-box")
        radio_button = self.radio_button_elements[index]
        if radio_button.find_elements(*box_locator):
            return RadioSelectItem(
                driver=self.web_driver,
                web_element=radio_button,
                box_locator=box_locator,
            )
        raise NoSuchElementException(f"Cannot find radio button at index {index}")

    def get_label(self, index: int) -> str:
        label_element = self.get_item_at_index(index)
        if label_element is None:
            raise NoSuchElementException(f"No label found at index {index}")
        return label_element.label

    def get_labels(self) -> list[str]:
        return [item.label for item in self.get_items()]

    def get_selected_label(self) -> str:
        for radio_button in self.get_items():
            if radio_button.is_selected():
                return radio_button.label
        return ""

    def select(self, labels_or_indexes: str | int | list[str] | list[int]) -> None:
        def select_by_index(index):
            radio = self.get_item_at_index(index)
            if radio is None:
                raise NoSuchElementException(f"No radio element found at index {index}")
            radio.select()

        def select_by_label(label):
            for item in self.get_items():
                if item.label == label:
                    item.select()
                    return

        if isinstance(labels_or_indexes, int):
            select_by_index(labels_or_indexes)
        elif isinstance(labels_or_indexes, str):
            select_by_label(labels_or_indexes)
        else:
            for label_or_index in labels_or_indexes:
                self.select(label_or_index)
