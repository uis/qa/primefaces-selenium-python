from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selocity import resilient_cached_webelement, resilient_cached_webelements

from primefaces.component.base.abstract_component import AbstractComponent
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions
from primefaces.primefaces_selenium import PrimefacesSelenium


class SplitButton(AbstractComponent):
    @property
    @resilient_cached_webelement
    def main_button_element(self) -> WebElement:
        return self.get_by_partial_id_from_base_element("_button")

    @property
    @resilient_cached_webelement
    def menu_button_element(self) -> WebElement:
        return self.get_by_partial_id_from_base_element("_menuButton")

    @property
    @resilient_cached_webelement
    def menu_element(self) -> WebElement:
        return self.web_driver.find_element(
            by=By.ID, value=f"{self.root_element.get_attribute('id')}_menu"
        )

    @property
    @resilient_cached_webelements
    def all_sub_menu_elements(self) -> list[WebElement]:
        return [
            e.find_element(by=By.TAG_NAME, value="a")
            for e in self.menu_element.find_elements(by=By.TAG_NAME, value="li")
            if PrimefacesSelenium(self.web_driver).element_has_css_classes(e, "ui-menuitem")
        ]

    def get_sub_menu_item_by_value(self, value: str) -> WebElement | None:
        return next(
            (sub_menu for sub_menu in self.all_sub_menu_elements if sub_menu.text == value),
            None,
        )

    def click(self):
        ps = PrimefacesSelenium(self.web_driver)
        ps.wait_gui().until(
            PrimefacesExpectedConditions().visible_and_animation_complete(self.main_button_element)
        )
        ps.wait_gui().until(expected_conditions.element_to_be_clickable(self.main_button_element))
        if self.main_button_element.get_attribute("data-pfconfirmcommand") is not None:
            # Confirm Dialog/Popup we don't want to guard for AJAX
            self.main_button_element.click()
        elif self.is_ajaxified("onclick"):
            ps.guard_ajax(self.main_button_element.click)
        elif self.main_button_element.get_attribute("type") == "submit":
            ps.guard_http(self.main_button_element.click)

    def click_menu(self):
        ps = PrimefacesSelenium(self.web_driver)
        ps.wait_gui().until(
            PrimefacesExpectedConditions().visible_and_animation_complete(self.menu_button_element)
        )
        ps.wait_gui().until(expected_conditions.element_to_be_clickable(self.menu_button_element))
        if self.is_ajaxified("onclick"):
            ps.guard_ajax(self.menu_button_element.click)
        else:
            self.menu_button_element.click()
        ps.wait_gui().until(
            lambda d: len(
                d.find_elements(
                    by=By.XPATH,
                    value=f"//div[@id='{self.root_dom_id}' and @aria-expanded='true']",
                )
            )
            > 0
        )

    def click_sub_menu_item(self, value: str):
        sub_menu = self.get_sub_menu_item_by_value(value)
        if sub_menu is None:
            raise NoSuchElementException("No sub-menu element found!")
        ps = PrimefacesSelenium(self.web_driver)
        ps.wait_gui().until(
            PrimefacesExpectedConditions().visible_and_animation_complete(sub_menu)
        )
        ps.wait_gui().until(expected_conditions.element_to_be_clickable(sub_menu))
        if self.is_ajaxified("onclick"):
            ps.guard_ajax(sub_menu.click)
        else:
            sub_menu.click()

    def click_sub_menu(self, menu_label: str):
        self.click_menu()
        self.click_sub_menu_item(menu_label)
