from selenium.webdriver.common.by import By
from selocity import resilient_cached_webelement

from primefaces.component.base.abstract_component import AbstractComponent


class ConfirmPopup(AbstractComponent):
    @property
    @resilient_cached_webelement
    def message(self):
        return self.root_element.find_element(By.CLASS_NAME, "ui-confirm-dialog-message")

    @property
    @resilient_cached_webelement
    def icon_element(self):
        return self.root_element.find_element(By.CLASS_NAME, "ui-confirm-dialog-severity")

    @property
    @resilient_cached_webelement
    def yes_button_element(self):
        return self.root_element.find_element(By.CLASS_NAME, "ui-confirmdialog-yes")

    @property
    @resilient_cached_webelement
    def no_button_element(self):
        return self.root_element.find_element(By.CLASS_NAME, "ui-confirmdialog-no")
