from enum import Enum, auto

from selenium.webdriver.common.by import By
from selocity import resilient_cached_webelement, resilient_cached_webelements

from primefaces.component.base.abstract_pageable_data import AbstractPageableData
from primefaces.primefaces_selenium import PrimefacesSelenium


class Layout(Enum):
    LIST = auto()
    GRID = auto()


class DataView(AbstractPageableData):
    @property
    @resilient_cached_webelement
    def header_element(self):
        return self.root_element.find_element(By.CLASS_NAME, "ui-dataview-header")

    @property
    @resilient_cached_webelement
    def content_element(self):
        return self.root_element.find_element(By.CLASS_NAME, "ui-dataview-content")

    @property
    @resilient_cached_webelement
    def element(self):
        return self.root_element.find_element(By.CLASS_NAME, "ui-dataview")

    @property
    @resilient_cached_webelement
    def layout_options_element(self):
        return self.header_element.find_element(By.CLASS_NAME, "ui-dataview-layout-options")

    @property
    @resilient_cached_webelements
    def rows_elements(self):
        if self.get_active_layout() == Layout.LIST:
            return self.content_element.find_elements(By.CLASS_NAME, "ui-dataview-row")
        return self.content_element.find_elements(By.CLASS_NAME, "ui-dataview-column")

    def get_active_layout(self) -> Layout | None:
        layout_buttons = self.layout_options_element.find_elements(By.CLASS_NAME, "ui-button")
        for layout_button in layout_buttons:
            layout_button_input_hidden = layout_button.find_element(By.TAG_NAME, "input")
            if layout_button_input_hidden.get_attribute("checked") == "true":
                if layout_button_input_hidden.get_attribute("value") == "list":
                    return Layout.LIST
                else:
                    return Layout.GRID
        return None

    def set_active_layout(self, layout: Layout):
        layout_buttons = self.layout_options_element.find_elements(By.CLASS_NAME, "ui-button")
        for layout_button in layout_buttons:
            layout_button_input_hidden = layout_button.find_element(By.TAG_NAME, "input")
            if (
                layout == Layout.LIST
                and layout_button_input_hidden.get_attribute("value") == "list"
            ):
                PrimefacesSelenium(self.web_driver).guard_ajax(layout_button.click)
            elif (
                layout == Layout.GRID
                and layout_button_input_hidden.get_attribute("value") == "grid"
            ):
                PrimefacesSelenium(self.web_driver).guard_ajax(layout_button.click)
