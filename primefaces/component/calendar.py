from datetime import datetime, timedelta

from selenium.webdriver import Keys
from selocity import resilient_cached_webelement

from primefaces.component.base.abstract_input_component import AbstractInputComponent
from primefaces.component.base.component_utils import ComponentUtils
from primefaces.primefaces_selenium import PrimefacesSelenium


class Calendar(AbstractInputComponent):
    @property
    @resilient_cached_webelement
    def input_element(self):
        return self.get_by_partial_id_from_base_element("_input")

    def is_date_select_ajaxified(self) -> bool:
        return ComponentUtils(self.web_driver).has_ajax_behavior(self.root_element, "dateSelect")

    def is_view_change_ajaxified(self) -> bool:
        return ComponentUtils(self.web_driver).has_ajax_behavior(self.root_element, "viewChange")

    def is_close_ajaxified(self) -> bool:
        return ComponentUtils(self.web_driver).has_ajax_behavior(self.root_element, "close")

    def get_value(self):
        ps = PrimefacesSelenium(self.web_driver)
        date = ps.execute_script(f"return {self.widget_id}.getDate()")
        if not date:
            return None
        time_zone_offset: int = int(
            ps.execute_script(f"return {self.widget_id}.getDate().getTimezoneOffset()")
        )
        utc_time_string = ps.execute_script(f"return {self.widget_id}.getDate().toUTCString()")
        rfc_1123_format = "%a, %d %b %Y %H:%M:%S %Z"
        parsed_datetime = datetime.strptime(utc_time_string, rfc_1123_format)
        return parsed_datetime - timedelta(minutes=time_zone_offset)

    def get_timezone_offset(self) -> float:
        return PrimefacesSelenium(self.web_driver).execute_script(
            "return new Date().getTimezoneOffset();"
        )

    def millis_as_formatted_date(self, millis: float):
        return PrimefacesSelenium(self.web_driver).execute_script(
            f"return $.datepicker.formatDate({self.widget_id}.cfg.dateFormat, new Date({millis}));"
        )

    def get_widget_data(self):
        return PrimefacesSelenium(self.web_driver).execute_script(
            f"return {self.widget_id}.getDate();"
        )

    def set_date(self, epoch):
        PrimefacesSelenium(self.web_driver).execute_script(
            f"{self.widget_id}.setDate(new Date({epoch}));"
        )

    def set_value(self, millis: float):
        ps = PrimefacesSelenium(self.web_driver)
        if ps.is_safari:
            self.set_date(millis)
        else:
            formatted_date = self.millis_as_formatted_date(millis)
            self.select_all_text()
            if self.is_view_change_ajaxified():
                ps.guard_ajax(self.input_element.send_keys, formatted_date)
            else:
                self.input_element.send_keys(Keys.TAB)
