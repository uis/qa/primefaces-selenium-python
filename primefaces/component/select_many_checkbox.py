from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selocity import resilient_cached_webelements

from primefaces.component.base.abstract_component import AbstractComponent
from primefaces.component.model.select_item import CheckboxSelectItem
from primefaces.primefaces_selenium import PrimefacesSelenium


class SelectManyCheckbox(AbstractComponent):
    @property
    @resilient_cached_webelements
    def checkbox_container_elements(self) -> list[WebElement]:
        root_tag = self.root_element.tag_name
        container_tag = "tr" if root_tag == "table" else "div"
        return self.root_element.find_elements(By.TAG_NAME, container_tag)

    @property
    @resilient_cached_webelements
    def checkbox_elements(self) -> list[WebElement]:
        _checkboxes = []
        for container in self.checkbox_container_elements:
            divs = container.find_elements(By.TAG_NAME, "div")
            for div in divs:
                if PrimefacesSelenium(self.web_driver).element_has_css_classes(div, "ui-chkbox"):
                    _checkboxes.append(div)
                    break
        return _checkboxes

    def get_items(self) -> list[CheckboxSelectItem]:
        box_locator = (By.CLASS_NAME, "ui-chkbox-box")
        return [
            CheckboxSelectItem(
                driver=self.web_driver,
                web_element=checkbox,
                box_locator=box_locator,
            )
            for checkbox in self.checkbox_container_elements
            if checkbox.find_elements(*box_locator)
        ]

    def get_item_at_index(self, index: int) -> CheckboxSelectItem | None:
        """Returns a CheckboxSelectItem at the given index efficiently, or None if out of range."""
        box_locator = (By.CLASS_NAME, "ui-chkbox-box")
        try:
            checkbox = self.checkbox_container_elements[index]
        except IndexError:
            return None
        if checkbox.find_elements(*box_locator):
            return CheckboxSelectItem(
                driver=self.web_driver,
                web_element=checkbox,
                box_locator=box_locator,
            )
        return None

    def toggle_all(self):
        for item in self.get_items():
            item.toggle()

    def deselect_all(self, ignore_indices: list[int] | None = None):
        ignore_indices = ignore_indices or []
        for index, item in enumerate(self.get_items()):
            if index not in ignore_indices:
                item.deselect()

    def select_all(self):
        for item in self.get_items():
            item.select()

    def get_label(self, index: int) -> str:
        checkbox = self.get_item_at_index(index)
        if checkbox is None:
            raise NoSuchElementException(f"No checkbox found at index {index}")
        return checkbox.label

    def get_labels(self) -> list[str]:
        return [item.label for item in self.get_items()]

    def select(self, labels_or_indexes: str | int | list[str] | list[int]) -> None:
        def select_by_index(index):
            checkbox = self.get_item_at_index(index)
            if checkbox is None:
                raise NoSuchElementException(f"No checkbox found at index {index}")
            checkbox.select()

        def select_by_label(label):
            for item in self.get_items():
                if item.label == label:
                    item.select()
                    return

        if isinstance(labels_or_indexes, int):
            select_by_index(labels_or_indexes)
        elif isinstance(labels_or_indexes, str):
            select_by_label(labels_or_indexes)
        else:
            for label_or_index in labels_or_indexes:
                self.select(label_or_index)
