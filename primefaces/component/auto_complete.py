import time
from functools import cached_property

from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selocity import resilient_cached_webelement, resilient_cached_webelements

from primefaces.component.base.abstract_input_component import AbstractInputComponent
from primefaces.component.base.component_utils import ComponentUtils
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions
from primefaces.primefaces_selenium import PrimefacesSelenium


class AutoComplete(AbstractInputComponent):
    def __init__(self, web_driver: WebDriver, base_element_locator: tuple[str, str]):
        super().__init__(web_driver, base_element_locator)
        self.tabs = None

    @property
    @resilient_cached_webelement
    def input_element(self) -> WebElement:
        return self.get_by_partial_id_from_base_element("_input")

    @property
    @resilient_cached_webelement
    def panel_element(self):
        return self.get_by_partial_id_from_base_element("_panel")

    @property
    @resilient_cached_webelement
    def drop_down_button_element(self):
        return self.root_element.find_element(By.CLASS_NAME, "ui-autocomplete-dropdown")

    @property
    @resilient_cached_webelements
    def item_elements(self) -> list[WebElement]:
        return self.web_driver.find_elements(By.CLASS_NAME, "ui-autocomplete-items")

    @property
    @resilient_cached_webelements
    def token_elements(self):
        return self.root_element.find_elements(By.CSS_SELECTOR, "ul li.ui-autocomplete-token")

    def clear(self):
        PrimefacesSelenium(self.web_driver).clear_input(
            self.input_element, self.is_clear_ajaxified()
        )

    def show(self):
        if self.is_enabled() and not self.panel_element.is_displayed():
            PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.show();")

    def wait_for_panel(self):
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            PrimefacesExpectedConditions().visible_and_animation_complete(self.panel_element)
        )
        self.wait_for_panel()

    def hide(self):
        if self.is_enabled() and self.panel_element.is_displayed():
            ps = PrimefacesSelenium(self.web_driver)
            ps.execute_script(f"{self.widget_id}.hide();")
            ps.wait_gui().until(
                PrimefacesExpectedConditions().invisible_and_animation_complete(self.panel_element)
            )

    def activate(self):
        PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.activate();")

    def deactivate(self):
        PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.deactivate();")

    def add_item(self, item: str):
        PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.addItem('{item}');")

    def remove_item(self, item: str):
        PrimefacesSelenium(self.web_driver).execute_script(
            f"{self.widget_id}.removeItem('{item}');"
        )

    def search(self, value: str):
        PrimefacesSelenium(self.web_driver).execute_script_ajaxified(
            f"{self.widget_id}.search(arguments[0]);", value
        )
        self.wait_for_panel()

    @cached_property
    def delay(self):
        return self.widget_configuration.get("delay")

    def get_item_values(self) -> list[str]:
        return [item.get_attribute("textContent") for item in self.item_elements]

    def get_value(self):
        return self.input_element.get_attribute("value")

    def is_clear_ajaxified(self):
        return ComponentUtils(self.web_driver).has_ajax_behavior(self.root_element, "clear")

    def is_item_select_ajaxified(self):
        return ComponentUtils(self.web_driver).has_ajax_behavior(self.root_element, "itemSelect")

    def is_item_unselect_ajaxified(self):
        return ComponentUtils(self.web_driver).has_ajax_behavior(self.root_element, "itemUnselect")

    def is_query_ajaxified(self):
        return ComponentUtils(self.web_driver).has_ajax_behavior(self.root_element, "query")

    def get_values(self):
        return [
            token.find_element(By.CLASS_NAME, "ui-autocomplete-token-label")
            for token in self.token_elements
        ]

    def send_tab_key(self):
        if self.is_on_change_ajaxified():
            PrimefacesSelenium(self.web_driver).guard_ajax(self.input_element.send_keys, Keys.TAB)
        else:
            self.input_element.send_keys(Keys.TAB)

    def set_value_without_tab(self, value) -> None:
        self.input_element.clear()
        time.sleep(self.delay * 0.005)
        self.input_element.send_keys(str(value))
        time.sleep(self.delay * 0.008)

    def set_value(self, value: str):
        self.set_value_without_tab(value)
        if self.delay > 0:
            PrimefacesSelenium(self.web_driver).wait_gui().until(
                PrimefacesExpectedConditions().animation_not_active()
            )
            time.sleep(self.delay * 0.005)
        self.send_tab_key()
