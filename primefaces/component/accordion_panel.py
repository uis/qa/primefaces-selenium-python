from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selocity import resilient_cached_webelements

from primefaces.component.base.abstract_component import AbstractComponent
from primefaces.component.base.component_utils import ComponentUtils
from primefaces.component.model.tab import Tab
from primefaces.primefaces_selenium import PrimefacesSelenium


class AccordionPanel(AbstractComponent):
    HEADERS = (By.CSS_SELECTOR, ".ui-accordion-header")
    CONTENTS = (By.CSS_SELECTOR, ".ui-accordion-content")

    def __init__(self, web_driver: WebDriver, base_element_locator: tuple[str, str]):
        super().__init__(web_driver, base_element_locator)
        self.tabs: list[Tab] = []

    @property
    @resilient_cached_webelements
    def header_elements(self):
        return self.root_element.find_elements(*self.HEADERS)

    @property
    @resilient_cached_webelements
    def content_elements(self):
        return self.root_element.find_elements(*self.CONTENTS)

    def get_tabs(self) -> list[Tab]:
        """Gets the accordion tabs."""
        if not self.tabs:
            self.tabs = [
                Tab(
                    header.get_attribute("textContent"),
                    index,
                    header,
                    self.content_elements[index],
                )
                for index, header in enumerate(self.header_elements)
            ]
        return self.tabs

    def toggle_tab(self, index: int):
        if ComponentUtils(self.web_driver).has_ajax_behavior(self.root_element, "tabChange"):
            PrimefacesSelenium(self.web_driver).guard_ajax(self.header_elements[index].click)
        else:
            self.header_elements[index].click()

    def expand_tab(self, index: int):
        tab = self.header_elements[index]
        if tab.get_attribute("aria-expanded").lower() == "false":
            self.toggle_tab(index)

    def collapse_tab(self, index: int):
        tab = self.header_elements[index]
        if tab.get_attribute("aria-expanded").lower() == "true":
            self.toggle_tab(index)

    def get_selected_tabs(self) -> list[Tab]:
        return [
            tab
            for tab in self.get_tabs()
            if "ui-state-active" in (tab.header_element.get_attribute("class") or "")
        ]
