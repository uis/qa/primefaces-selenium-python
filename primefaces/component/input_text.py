from selenium.webdriver import Keys

from primefaces.component.base.abstract_input_component import AbstractInputComponent
from primefaces.primefaces_selenium import PrimefacesSelenium


class InputText(AbstractInputComponent):
    def get_value(self):
        return self.input_element.get_attribute("value")

    def set_value(self, value: str):
        ajaxified = self.is_on_change_ajaxified()
        if self.get_value():
            if ajaxified:
                PrimefacesSelenium(self.web_driver).guard_ajax(self.input_element.clear)
            else:
                self.input_element.clear()
        self.input_element.send_keys(value)
        if ajaxified:
            PrimefacesSelenium(self.web_driver).guard_ajax(self.input_element.send_keys, Keys.TAB)
        else:
            self.input_element.send_keys(Keys.TAB)
