from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.event_firing_webdriver import EventFiringWebDriver
from selocity import resilient_cached_webelements

from primefaces.component.base.abstract_component import AbstractComponent
from primefaces.component.base.component_utils import ComponentUtils
from primefaces.component.model.tab import Tab
from primefaces.primefaces_selenium import PrimefacesSelenium


class HeaderLabelNotFoundError(BaseException):
    pass


class TabView(AbstractComponent):
    HEADERS = (By.CSS_SELECTOR, ".ui-tabs-header")
    OLD_HEADERS = (By.XPATH, "//*[@role='tab']")
    CONTENTS = (By.CSS_SELECTOR, ".ui-tabs-panel")
    SELECTED_TAB_CLASS = "ui-tabs-selected"

    def __init__(
        self,
        web_driver: WebDriver | EventFiringWebDriver,
        base_element_locator: tuple[str, str],
    ):
        super().__init__(web_driver, base_element_locator)
        self._tabs: list[Tab] = []

    @property
    @resilient_cached_webelements
    def header_elements(self) -> list[WebElement]:
        return self.root_element.find_elements(*self.HEADERS) or self.root_element.find_elements(
            *self.OLD_HEADERS
        )

    @property
    @resilient_cached_webelements
    def content_elements(self) -> list[WebElement]:
        return self.root_element.find_elements(*self.CONTENTS)

    def get_tabs(self) -> list[Tab]:
        """
        Gets the tab view tabs.
        """
        if not self._tabs:
            self._tabs = [
                Tab(
                    header.find_element(By.XPATH, ".//*[normalize-space(text())][1]").text,
                    index,
                    header,
                    self.content_elements[index],
                )
                for index, header in enumerate(self.header_elements)
            ]
        return self._tabs

    def toggle_tab(self, index: int):
        cfg = self.widget_configuration
        is_dynamic = cfg.get("dynamic", False)
        tab_to_select = self.header_elements[index]

        if is_dynamic and ComponentUtils(self.web_driver).has_ajax_behavior(
            self.root_element, "tabChange"
        ):
            PrimefacesSelenium(self.web_driver).guard_ajax(tab_to_select.click)
        else:
            tab_to_select.click()

        # Wait until the tab is selected
        PrimefacesSelenium(self.web_driver).wait_gui().until(
            lambda driver: PrimefacesSelenium(driver).has_css_class(
                tab_to_select, self.SELECTED_TAB_CLASS
            )
        )

    def get_tab_by_label(self, label: str):
        match_tabs = [tab for tab in self.get_tabs() if tab.title == label]
        if not match_tabs:
            raise NoSuchElementException(f"No tabs found with label {label}")
        return match_tabs[0]

    def toggle_tab_by_label(self, label: str):
        toggle_tab = self.get_tab_by_label(label)
        self.toggle_tab(toggle_tab.index)

    def get_selected_tab(self) -> Tab | None:
        for tab in self.get_tabs():
            if PrimefacesSelenium(self.web_driver).has_css_class(
                tab.header_element, self.SELECTED_TAB_CLASS
            ):
                return tab
        return None
