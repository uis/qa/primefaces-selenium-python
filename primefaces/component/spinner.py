from selenium.webdriver.common.by import By
from selocity import resilient_cached_webelement

from primefaces.component.input_text import InputText
from primefaces.primefaces_selenium import PrimefacesSelenium


class Spinner(InputText):
    @property
    @resilient_cached_webelement
    def input_element(self):
        return self.root_element.find_element(By.CSS_SELECTOR, "[id*='_input']")

    @property
    @resilient_cached_webelement
    def button_up_element(self):
        return self.root_element.find_element(By.CSS_SELECTOR, ".ui-spinner-up")

    @property
    @resilient_cached_webelement
    def button_down_element(self):
        return self.root_element.find_element(By.CSS_SELECTOR, ".ui-spinner-down")

    def get_value(self):
        return self.input_element.get_attribute("value")

    def set_value(self, value):
        if value is None:
            value = '""'
        PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.setValue({value})")

    def increment(self):
        PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.spin(1);")

    def decrement(self):
        PrimefacesSelenium(self.web_driver).execute_script(f"{self.widget_id}.spin(-1);")

    def change(self):
        if self.is_on_change_ajaxified():
            PrimefacesSelenium(self.web_driver).execute_script_ajaxified(
                f"{self.widget_id}.input.trigger('change');"
            )
        else:
            PrimefacesSelenium(self.web_driver).execute_script(
                f"{self.widget_id}.input.trigger('change');"
            )
