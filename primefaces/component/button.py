from selenium.webdriver.support import expected_conditions

from primefaces.component.base.abstract_component import AbstractComponent
from primefaces.primefaces_expected_conditions import PrimefacesExpectedConditions
from primefaces.primefaces_selenium import PrimefacesSelenium


class Button(AbstractComponent):
    def click(self):
        button = self.root_element
        ps = PrimefacesSelenium(self.web_driver)
        ps.wait_gui().until(PrimefacesExpectedConditions().visible_and_animation_complete(button))
        ps.wait_gui().until(expected_conditions.element_to_be_clickable(button))
        if button.get_attribute("data-pfconfirmcommand") is not None:
            # Confirm Dialog/Popup we don't want to guard for AJAX
            button.click()
        elif self.is_ajaxified("onclick"):
            ps.guard_ajax(button.click)
        elif button.get_attribute("type") == "submit":
            ps.guard_http(button.click)
        else:
            button.click()
