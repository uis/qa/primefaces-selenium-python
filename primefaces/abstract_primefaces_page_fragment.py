import logging
from functools import cached_property
from typing import cast

from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.event_firing_webdriver import EventFiringWebDriver
from selocity import resilient_cached_webelement

from primefaces.internal.event_listener import EventListener
from primefaces.internal.onload_scripts import OnloadScripts

LOGGER = logging.getLogger(__name__)


class AbstractPrimefacesPageFragment:
    def __init__(
        self,
        web_driver: WebDriver | EventFiringWebDriver,
        base_element_locator: tuple[str, str],
    ):
        self._in_init = True
        _web_driver = (
            web_driver
            if isinstance(web_driver, EventFiringWebDriver)
            else EventFiringWebDriver(driver=web_driver, event_listener=EventListener())
        )
        self.web_driver: WebDriver = cast(WebDriver, _web_driver)
        self.base_element_locator = base_element_locator
        OnloadScripts(self.web_driver).execute()

    @property
    @resilient_cached_webelement
    def root_element(self) -> WebElement:
        return self.web_driver.find_element(*self.base_element_locator)

    @cached_property
    def root_dom_id(self) -> str:
        return self.root_element.get_attribute("id")

    def get_by_partial_id_from_root(self, partial_id: str) -> WebElement:
        xpath = f"//*[contains(@id, '{partial_id}') and contains(@id, '{self.root_dom_id}')][1]"
        elements = self.root_element.find_elements(By.XPATH, xpath)

        if elements:
            return elements[0]  # Return the first matching element

        raise NoSuchElementException(f"No element with partial id '{partial_id}' found.")

    def get_by_partial_id_from_base_element(self, partial_id: str) -> WebElement:
        xpath = f".//*[contains(@id, '{partial_id}')]"
        elements = self.root_element.find_elements(By.XPATH, xpath)

        if elements:
            return elements[0]  # Return the first matching element

        raise NoSuchElementException(f"No element with partial id '{partial_id}' found.")

    def is_displayed(self) -> bool:
        return self.root_element.is_displayed()

    def is_enabled(self) -> bool:
        return self.root_element.is_enabled()
