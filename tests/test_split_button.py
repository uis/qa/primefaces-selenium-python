from selenium.webdriver.common.by import By

from primefaces.component.split_button import SplitButton


class TestSplitButton:
    def test_split_button_main_button_click(self, driver, page_config):
        driver.get(page_config["splitButton"])
        base_element = (
            By.XPATH,
            "(//div[contains(@class, 'ui-splitbutton') and @aria-controls])[1]",
        )
        split_button = SplitButton(driver, base_element)
        split_button.click()

    def test_split_button_sub_menu_click(self, driver, page_config):
        driver.get(page_config["splitButton"])
        base_element = (
            By.XPATH,
            "(//div[contains(@class, 'ui-splitbutton') and @aria-controls])[1]",
        )
        split_button = SplitButton(driver, base_element)
        split_button.click_sub_menu("Delete")
