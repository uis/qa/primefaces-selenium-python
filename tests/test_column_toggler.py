from selenium.webdriver.common.by import By

from primefaces.component.column_toggler import ColumnToggler


class TestColumnToggler:
    def test_select_by_label(self, driver, page_config):
        driver.get(page_config["columnToggler"])
        base_element = (
            By.XPATH,
            "//button[contains(@id, ':products:toggler')][1]",
        )
        column_toggler = ColumnToggler(driver, base_element)
        column_toggler.show_toggle_menu()
        _header_text = "Quantity"
        column_toggler.toggle(column_toggler.get_index_for_label(_header_text))

    def test_select_by_index(self, driver, page_config):
        driver.get(page_config["columnToggler"])
        base_element = (
            By.XPATH,
            "//button[contains(@id, ':products:toggler')][1]",
        )
        column_toggler = ColumnToggler(driver, base_element)
        column_toggler.show_toggle_menu()
        _header_index = 3
        column_toggler.select(_header_index)
