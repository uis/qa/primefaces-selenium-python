from selenium.webdriver.common.by import By

from primefaces.component.input_number import InputNumber


class TestInputNumber:
    def test_input_number(self, driver, page_config):
        driver.get(page_config["inputNumber"])
        base_element = (By.CSS_SELECTOR, ".ui-inputnumber")
        input_num = InputNumber(driver, base_element)
        val = 15
        input_num.set_value(str(val))
        assert int(float(input_num.get_widget_value())) == val
