from selenium.webdriver.common.by import By

from primefaces.component.spinner import Spinner


class TestSpinner:
    def test_spinner_set_value(self, driver, page_config):
        driver.get(page_config["spinner"])
        base_element = (
            By.CSS_SELECTOR,
            "[id$=':basic']",
        )
        spinner = Spinner(driver, base_element)
        _value = 20
        spinner.set_value(str(_value))

        assert spinner.get_value() == str(_value)

    def test_spinner_increment_button(self, driver, page_config):
        driver.get(page_config["spinner"])
        base_element = (
            By.CSS_SELECTOR,
            "[id$=':basic']",
        )
        spinner = Spinner(driver, base_element)
        _value = 10
        spinner.set_value(str(_value))
        spinner.increment()
        assert spinner.get_value() == str(_value + 1)

    def test_spinner_decrement_button(self, driver, page_config):
        driver.get(page_config["spinner"])
        base_element = (
            By.CSS_SELECTOR,
            "[id$=':basic']",
        )
        spinner = Spinner(driver, base_element)
        _value = 10
        spinner.set_value(str(_value))
        spinner.decrement()
        assert spinner.get_value() == str(_value - 1)

    def test_change_trigger(self, driver, page_config):
        driver.get(page_config["spinner"])
        base_element = (
            By.CSS_SELECTOR,
            "[id$=':basic']",
        )
        spinner = Spinner(driver, base_element)
        spinner.change()
