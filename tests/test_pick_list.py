import time

from selenium.webdriver.common.by import By

from primefaces.component.pick_list import PickList


class TestPickList:
    def test_pick_all_countries(self, driver, page_config):
        driver.get(page_config["pickList"])
        base_element = (
            By.XPATH,
            "//*[contains(@id, ':pojoPickList') and contains(@class, 'ui-picklist ')]",
        )
        pick_list = PickList(driver, base_element)
        total_countries = len(pick_list.get_source_list_elements())
        pick_list.pick_all()
        assert len(pick_list.get_target_list_elements()) == total_countries

    def test_pick_some_countries(self, driver, page_config):
        driver.get(page_config["pickList"])
        base_element = (
            By.XPATH,
            "//*[contains(@id, ':pojoPickList') and contains(@class, 'ui-picklist ')]",
        )
        pick_list = PickList(driver, base_element)
        pick_list.pick("Algeria", "Andorra")
        assert len(pick_list.get_target_list_elements()) == 2

    def test_filter_in_source_list(self, driver, page_config):
        driver.get(page_config["pickList"])
        base_element = (
            By.XPATH,
            "//*[contains(@id, ':pojoPickList') and contains(@class, 'ui-picklist ')]",
        )
        pick_list = PickList(driver, base_element)
        pick_list.filter_source_list("Al")
        time.sleep(1)
        assert len(pick_list.get_filtered_source_list_elements()) == 2

    def test_filter_in_target_list(self, driver, page_config):
        driver.get(page_config["pickList"])
        base_element = (
            By.XPATH,
            "//*[contains(@id, ':pojoPickList') and contains(@class, 'ui-picklist ')]",
        )
        pick_list = PickList(driver, base_element)
        pick_list.pick("Algeria", "Andorra")
        pick_list.filter_target_list("Al")
        time.sleep(1)
        assert len(pick_list.get_filtered_target_list_elements()) == 1
