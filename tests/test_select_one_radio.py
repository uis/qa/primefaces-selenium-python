from selenium.webdriver.common.by import By

from primefaces.component.select_one_radio import SelectOneRadio


class TestSelectOneRadio:
    def test_radio_click(self, driver, page_config):
        driver.get(page_config["selectOneRadio"])
        base_element = (
            By.XPATH,
            "(//div[contains(@id, ':line') and @role='radiogroup'])[1]",
        )
        select_one = SelectOneRadio(driver, base_element)
        _option = "Option1"
        select_one.select(_option)
        assert select_one.get_selected_label() == _option
