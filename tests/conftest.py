import contextlib
import json
import os
from collections.abc import Generator
from pathlib import Path

import pytest
from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.safari.options import Options as SafariOptions


@pytest.fixture(scope="session")
def test_browser(request):
    """:returns browser name from --browser option"""
    return request.config.getoption("--browser")


@pytest.fixture(scope="session")
def remote_grid(request):
    """:returns grid URL from --grid option"""
    return request.config.getoption("--grid")


@pytest.fixture
def driver(remote_grid, test_browser) -> Generator[WebDriver, None, None]:
    with get_driver(test_browser, remote_grid) as driver:
        yield driver


@pytest.fixture(scope="session")
def page_config():
    with open(Path(__file__).parent / "config.json") as config:
        config_data = json.load(config)
        return config_data["testSite"]


@contextlib.contextmanager
def get_driver(test_browser, remote_grid_url=None) -> Generator[WebDriver, None, None]:
    if not remote_grid_url:
        _options = webdriver.FirefoxOptions()
        _options.accept_insecure_certs = True
        _service = Service(log_path=os.devnull)
        _browser = webdriver.Firefox(options=_options, service=_service)
    elif test_browser == "chrome":
        _options = webdriver.ChromeOptions()
        _options.accept_insecure_certs = True
        _browser = webdriver.Remote(options=_options, command_executor=remote_grid_url)
    elif test_browser == "firefox":
        _browser = webdriver.Remote(
            options=webdriver.FirefoxOptions(), command_executor=remote_grid_url
        )
    elif test_browser == "edge":
        _browser = webdriver.Remote(
            options=webdriver.EdgeOptions(), command_executor=remote_grid_url
        )
    else:
        _browser = webdriver.Remote(options=SafariOptions(), command_executor=remote_grid_url)

    _browser.maximize_window()
    yield _browser
    _browser.quit()


def pytest_addoption(parser):
    """Parse pytest --option variables from shell"""
    parser.addoption(
        "--browser",
        help="Which test browser? Choose from chrome, firefox, edge or safari",
        choices=["chrome", "firefox", "edge", "safari"],
        default="firefox",
    )
    parser.addoption(
        "--grid",
        help="Remote grid URL (if this is not provided, tests will run locally)",
        action="store",
        default="",
    )
