from selenium.webdriver.common.by import By

from primefaces.component.text_editor import TextEditor


class TestTextEditor:
    def test_text_editor(self, driver, page_config):
        driver.get(page_config["textEditor"])
        base_element = (By.CSS_SELECTOR, ".ui-texteditor")
        text_editor = TextEditor(driver, base_element)
        val = "Test Description"
        text_editor.set_value(val)
        assert val in text_editor.get_editor_value()
