from selenium.webdriver.common.by import By

from primefaces.component.tab_view import TabView


class TestTabView:
    def test_toggle_tab_based_on_index(self, driver, page_config):
        driver.get(page_config["tabView"])
        base_element = (By.XPATH, "//div[@data-widget][1]")
        tab_view = TabView(driver, base_element)
        _tab_label = "Header III"
        tab_view.toggle_tab(2)
        assert tab_view.get_selected_tab().title == _tab_label

    def test_toggle_tab_based_on_label(self, driver, page_config):
        driver.get(page_config["tabView"])
        base_element = (By.XPATH, "//div[@data-widget][1]")
        tab_view = TabView(driver, base_element)
        _tab_label = "Header II"
        tab_view.toggle_tab_by_label(_tab_label)
        assert tab_view.get_selected_tab().title == _tab_label
