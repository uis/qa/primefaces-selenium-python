from selenium.webdriver.common.by import By

from primefaces.component.data_table import DataTable


class TestDataTable:
    def test_data_table_column_index(self, driver, page_config):
        driver.get(page_config["dataTable"])
        base_element = (
            By.XPATH,
            "(//div[contains(@class, 'ui-datatable') and contains(@class, 'ui-widget')])[1]",
        )
        data_table = DataTable(driver, base_element)
        _header_text = "Category"
        _header_index = 2
        assert data_table.get_column_index_by_header_label(_header_text) == _header_index

    def test_paginator_buttons(self, driver, page_config):
        driver.get(page_config["dataTablePaginator"])
        base_element = (By.CLASS_NAME, "ui-datatable")
        data_table = DataTable(driver, base_element)
        data_table.next_page()
        assert data_table.paginator.get_active_page().number == 2
        data_table.previous_page()
        assert data_table.paginator.get_active_page().number == 1
        total_pages = len(data_table.paginator.get_pages())
        data_table.last_page()
        assert data_table.paginator.get_active_page().number == total_pages
        data_table.first_page()
        assert data_table.paginator.get_active_page().number == 1

    def test_rows_per_page_button(self, driver, page_config):
        driver.get(page_config["dataTablePaginator"])
        base_element = (By.CLASS_NAME, "ui-datatable")
        data_table = DataTable(driver, base_element)
        data_table.select_rows_per_page("5")
        assert len(data_table.paginator.get_pages()) == 10

    def test_filter_columns_function(self, driver, page_config):
        driver.get(page_config["dataTableFilter"])
        base_element = (By.CLASS_NAME, "ui-datatable")
        data_table = DataTable(driver, base_element)
        data_table.filter(filter_value="or", header_text="Name")
        data_table.filter(filter_value="NEW", header_text="Status")
        assert True
