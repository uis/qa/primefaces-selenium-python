from selenium.webdriver.common.by import By

from primefaces.component.select_one_menu import SelectOneMenu


class TestSelectOneMenu:
    def test_select_one_menu_basic_click(self, driver, page_config):
        driver.get(page_config["selectOneMenu"])
        base_element = (
            By.XPATH,
            "(//div[contains(@class, 'ui-selectonemenu') and contains(@id, 'option')])[1]",
        )
        select_one = SelectOneMenu(driver, base_element)
        _option = "Option1"
        select_one.select(_option)
        assert select_one.get_selected_label() == _option
