from selenium.webdriver.common.by import By

from primefaces.component.carousel import Carousel


class TestCarousel:
    def test_next_prev_page_buttons(self, driver, page_config):
        driver.get(page_config["carousel"])
        base_element = (By.XPATH, "//div[contains(@class, 'ui-carousel')][1]")
        carousel = Carousel(driver, base_element)
        assert carousel.get_item_by_index(0).is_displayed()
        assert not carousel.get_item_by_index(3).is_displayed()
        carousel.go_to_next_page()
        assert not carousel.get_item_by_index(0).is_displayed()
        assert carousel.get_item_by_index(3).is_displayed()
        carousel.go_to_previous_page()
        assert carousel.get_item_by_index(0).is_displayed()
        assert not carousel.get_item_by_index(5).is_displayed()

    def test_get_items_method(self, driver, page_config):
        driver.get(page_config["carousel"])
        base_element = (By.XPATH, "//div[contains(@class, 'ui-carousel')][1]")
        carousel = Carousel(driver, base_element)
        assert len(carousel.item_elements) == 9

    def test_get_pages_method(self, driver, page_config):
        driver.get(page_config["carousel"])
        base_element = (By.XPATH, "//div[contains(@class, 'ui-carousel')][1]")
        carousel = Carousel(driver, base_element)
        assert len(carousel.page_elements) == 3
